# WhoLacksTheVax

WhoLacksTheVax is a simple application for managing vaccinations, tests and certifications for facilities. You are currently viewing back-end side of things.

## Table of contents
- [Testing](#testing)
- [Documentation](#documentation)
- [Running](#running)

### Testing

If you are using [Insomnia](https://insomnia.rest/), there is [insomnia.json](https://github.com/BranislavBily/wholacksthevax/blob/master/insomnia.json) file.
Simply import it and you will see all possible requests that are currently available.

If you are using [Postman](https://www.postman.com/), you can import [wholacksthevax.yaml](https://github.com/BranislavBily/wholacksthevax/blob/master/wholacksthevax.yaml) file.

(Warning: there is usually a small delay between implementation and documentation. Please be patient.)

### Documentation

Documentation is written in OpenAPI in [wholacksthevax.yaml](https://github.com/BranislavBily/wholacksthevax/blob/master/wholacksthevax.yaml) file. Use IntelliJ plugin, swagger or any other editor for OpenAPI.

### Running
First run  ``./gradlew build`` to build .jar file. If you get error ``./gradlew not found``, new -> from existing sources -> gradle -> all the nexts.
Then ``docker build -t wholacksthevaxbe`` to create wholacksthevax back-end image. Lastly ``docker compose up`` to run the image in a container.
