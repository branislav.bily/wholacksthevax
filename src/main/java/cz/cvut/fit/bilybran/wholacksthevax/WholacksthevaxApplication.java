package cz.cvut.fit.bilybran.wholacksthevax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WholacksthevaxApplication {

	public static void main(String[] args) {
		SpringApplication.run(WholacksthevaxApplication.class, args);
	}

}
