package cz.cvut.fit.bilybran.wholacksthevax.controller;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberView;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto.VaccinationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.service.staff_member.StaffMemberService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@AllArgsConstructor(onConstructor_ = {@Autowired})
@RequestMapping("api/staffMember")
public class StaffMemberController {

    private StaffMemberService service;

    @PostMapping
    @JsonView(StaffMemberView.Recursive.class)
    public StaffMemberDTO postStaffMember(@Validated(StaffMemberView.Recursive.class) @RequestBody StaffMemberDTO staffMemberDTO) {
        return service.postMember(staffMemberDTO);
    }

    @GetMapping
    @JsonView(StaffMemberView.Recursive.class)
    public Collection<StaffMemberDTO> listAll() {
        return service.listAll();
    }

    @GetMapping("{id}")
    @JsonView(StaffMemberView.Recursive.class)
    public StaffMemberDTO getStaffMember(@PathVariable Long id) {
        return service.getStaffMember(id);
    }

    @GetMapping("{id}/tests")
    @JsonView(StaffMemberView.Recursive.class)
    public Collection<TestDTO> getStaffMemberTests(@PathVariable Long id) {
        return service.getTests(id);
    }

    @GetMapping("{id}/vaccinations")
    @JsonView(StaffMemberView.Recursive.class)
    public Collection<VaccinationDTO> getStaffMemberVaccinations(@PathVariable Long id) {
        return service.getVaccinations(id);
    }

    @PostMapping("{member_id}/{facility_id}")
    @JsonView(StaffMemberView.Recursive.class)
    public StaffMemberDTO addFacility(@PathVariable(value = "member_id") Long memberId, @PathVariable(value = "facility_id") Long facilityId) {
        return service.addFacility(memberId, facilityId);
    }

    @DeleteMapping("{member_id}/{facility_id}")
    @JsonView(StaffMemberView.Recursive.class)
    public StaffMemberDTO removeFacility(@PathVariable(value = "member_id") Long memberId, @PathVariable(value = "facility_id") Long facilityId) {
        return service.removeFacility(memberId, facilityId);
    }

    @GetMapping("{member_id}/{facility_id}/colleagues")
    @JsonView(StaffMemberView.Recursive.class)
    public Collection<StaffMemberDTO> getColleagues(@PathVariable(value = "member_id") Long memberId, @PathVariable(value = "facility_id") Long facilityId) {
        return service.getColleagues(memberId, facilityId);
    }

    @PutMapping("{id}")
    @JsonView(StaffMemberView.Recursive.class)
    public StaffMemberDTO updateStaffMember(@PathVariable Long id, @Validated(StaffMemberView.Recursive.class) @RequestBody StaffMemberDTO staffMemberDTO) {
        return service.updateStaffMember(id, staffMemberDTO);
    }

    @DeleteMapping("{id}")
    @JsonView(StaffMemberView.Recursive.class)
    public void deleteStaffMember(@PathVariable Long id) {
        service.deleteStaffMember(id);
    }

}
