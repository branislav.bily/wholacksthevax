package cz.cvut.fit.bilybran.wholacksthevax.model.person.dto;


import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerView;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityView;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Gender;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Insurance;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Person;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberView;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Value
@AllArgsConstructor
@EqualsAndHashCode
public class PersonDTO {

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, StaffMemberView.Recursive.class, FacilityView.Recursive.class, CustomerView.Tests.class, CustomerView.Vaccinations.class})
    @NotEmpty(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class, StaffMemberView.Recursive.class})
    String firstName;

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, StaffMemberView.Recursive.class, FacilityView.Recursive.class, CustomerView.Tests.class, CustomerView.Vaccinations.class})
    @NotEmpty(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class, StaffMemberView.Recursive.class})
    String secondName;

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, StaffMemberView.Recursive.class, FacilityView.Recursive.class})
    @NotEmpty(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class, StaffMemberView.Recursive.class})
    String socialSecurityNumber;

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, StaffMemberView.Recursive.class, FacilityView.Recursive.class})
    @NotNull(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class, StaffMemberView.Recursive.class})
    //yyyy-MM-dd
    Date dateOfBirth;

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, StaffMemberView.Recursive.class, FacilityView.Recursive.class})
    @NotNull(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class, StaffMemberView.Recursive.class})
    Gender gender;

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, StaffMemberView.Recursive.class, FacilityView.Recursive.class})
    @NotNull(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class, StaffMemberView.Recursive.class})
    Insurance insurance;

    /**
     * Converts DTO into Model
     *
     * @return Model
     */
    public Person getPerson() {
        return new Person(
                firstName,
                secondName,
                socialSecurityNumber,
                dateOfBirth,
                gender,
                insurance
        );
    }
}
