package cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto;

public interface CustomerView {

    interface Info extends Vaccinations, Certifications, Tests {}

    interface LogIn {}

    interface SignUp extends LogIn {}

    interface UpdatePersonAddress {}

    interface Tests {}

    interface Vaccinations {}

    interface Certifications {}
}
