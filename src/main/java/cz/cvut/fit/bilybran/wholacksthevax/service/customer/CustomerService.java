package cz.cvut.fit.bilybran.wholacksthevax.service.customer;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.customer.*;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.Authentication;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.Customer;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.PasswordChange;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.PasswordChangeDTO;
import cz.cvut.fit.bilybran.wholacksthevax.repository.CustomerRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;
import java.util.Optional;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class CustomerService {

    CustomerRepository repository;

    /**
     * Sign up Customer
     *
     * @param customerDTO CustomerDTO that will be signed up
     */
    public void signUpCustomer(CustomerDTO customerDTO) {
        System.out.println(Timestamp.from(Instant.now()) + "  Sign up customer.");
        Customer customer = customerDTO.getCustomer();
        //Hopefully correct sign up logic
        if (repository.existsByEmail(customer.getEmail())) {
            System.out.println(Timestamp.from(Instant.now()) + "  Email already in use.");
            throw new EmailAlreadyUsedException();
        }
        if (repository.existsByPersonAndAddress(customer.getPerson(),
                customer.getAddress())) {
            System.out.println(Timestamp.from(Instant.now()) + "  Customer already exists.");
            throw new CustomerAlreadyExistsException();
        }
        repository.save(customer);
        System.out.println(Timestamp.from(Instant.now()) + "  Customer signed up.");
    }

    /**
     * Authenticate Customer
     *
     * @param authenticationDTO login credentials
     * @return id of logged in Customer
     */
    public long authenticateCustomer(AuthenticationDTO authenticationDTO) {
        Authentication authentication = authenticationDTO.getModel();
        System.out.println(Timestamp.from(Instant.now()) + "  Login customer.");
        if (!repository.existsByEmailAndPassword(authentication.getEmail(), authentication.getPassword())) {
            System.out.println(Timestamp.from(Instant.now()) + "  Invalid login credentials.");
            throw new IncorrectLoginCredentialsException();
        }
        Customer customer = repository.findByEmailAndPassword(authentication.getEmail(), authentication.getPassword());
        System.out.println(Timestamp.from(Instant.now()) + "  Logged in.");
        return customer.getCustomer_id();
    }

    /**
     * Authenticates Login credentials and compares if credentials were from correct Customer
     *
     * @param authenticationDTO login credentials
     * @param id                identification of Customer
     */
    public void authenticateCustomerWithID(AuthenticationDTO authenticationDTO, Long id) {
        Long authenticationId = authenticateCustomer(authenticationDTO);
        if (!authenticationId.equals(id)) {
            throw new IncorrectLoginCredentialsException();
        }
    }

    /**
     * Deletes Customer by a given ID
     *
     * @param id Customer's identifier
     */
    public void deleteCustomer(Long id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Deleting customer.");
        Customer customer = getCustomerById(id);
        repository.delete(customer);
        System.out.println(Timestamp.from(Instant.now()) + "  Customer deleted.");
    }

    /**
     * Gets Customer by a given ID
     *
     * @param id Customer's identifier
     * @return Customer with given ID
     */
    public CustomerDTO getCustomer(Long id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Getting customer with id " + id.toString() + " .");
        Customer customer = getCustomerById(id);
        return customer.getDTO();
    }

    /**
     * Lists all Customers
     *
     * @return Collection of all Customers
     */
    public Collection<CustomerDTO> listAll() {
        System.out.println(Timestamp.from(Instant.now()) + "  Listing all customers.");
        return Customer.mapCustomerToDTO(repository.findAll());
    }

    /**
     * Updates Customer with given ID with new data from parameters
     *
     * @param id          Customer's identifier
     * @param customerDTO new Customer data
     * @return updated Customer
     */
    @Transactional
    public CustomerDTO updateCustomer(Long id, CustomerDTO customerDTO) {
        System.out.println(Timestamp.from(Instant.now()) + "  Updating customer with id " + id.toString() + " .");
        Customer customer = getCustomerById(id);
        Customer newCustomer = customerDTO.getCustomer();
        customer.setAddress(newCustomer.getAddress());
        customer.setPerson(newCustomer.getPerson());
        return customer.getDTO();
    }

    /**
     * Updates password of Customer with given ID
     *
     * @param id                Customer's identifier
     * @param passwordChangeDTO previous and new passwords
     */
    @Transactional
    public void updatePassword(Long id, PasswordChangeDTO passwordChangeDTO) {
        System.out.println(Timestamp.from(Instant.now()) + "  Updating customer password " + id.toString() + " .");
        Customer customer = getCustomerById(id);
        PasswordChange passwordChange = passwordChangeDTO.getPasswordChange();
        if (customer.getPassword().equals(passwordChange.getPreviousPassword())) {
            customer.setPassword(passwordChange.getNewPassword());
        } else {
            throw new IncorrectPasswordException();
        }
    }

    /**
     * Gets Customer by a given ID
     *
     * @param id Customer's identifier
     * @return Customer with given ID
     */
    public Customer getCustomerById(Long id) {
        Optional<Customer> customer = repository.findById(id);
        if (customer.isEmpty()) {
            System.out.println(Timestamp.from(Instant.now()) + "  Customer with given id does not exists.");
            throw new UserWithGivenIDDoesNotExistsException();
        }
        return customer.get();
    }
}
