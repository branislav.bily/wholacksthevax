package cz.cvut.fit.bilybran.wholacksthevax.model.staffmember;

import cz.cvut.fit.bilybran.wholacksthevax.model.address.Address;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.Facility;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Person;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.Test;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.Vaccination;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "staff_member")
public class StaffMember {

    @Id
    @GeneratedValue
    private Long staffMemberId;

    @Embedded
    private Person person;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Facility> worksAt;

    @Embedded
    private Address address;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Test> tests;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Vaccination> vaccinations;

    /**
     * Adds Facility into registry of Facilities
     *
     * @param facility new Facility
     * @return true if Staff member was not previously working for given Facility
     */
    public boolean addFacility(Facility facility) {
        return worksAt.add(facility);
    }

    /**
     * Removes Facility from registry of Facilities
     *
     * @param facility Facility that will be removed
     */
    public void removeFacility(Facility facility) {
        worksAt.remove(facility);
    }

    /**
     * Adds Test into registry of Tests
     *
     * @param test new Test
     */
    public void addTest(Test test) {
        tests.add(test);
    }

    /**
     * Adds Vaccination into registry of Vaccinations
     *
     * @param vaccination new Vaccination
     */
    public void addVaccination(Vaccination vaccination) {
        vaccinations.add(vaccination);
    }

    /**
     * Check if Facility is employing Staff member
     *
     * @param facility Facility that employment is in question
     * @return true if Staff member is working for given Facility
     */
    public boolean worksAt(Facility facility) {
        return worksAt.contains(facility);
    }

    /**
     * Creates DTO from sources with zero Facilities
     *
     * @return DTO without Facilities
     */
    public StaffMemberDTO getDTOWithoutFacilities() {
        return new StaffMemberDTO(
                staffMemberId,
                person.getDTO(),
                Collections.EMPTY_SET,
                address.getDTO()
        );
    }

    /**
     * Creates DTO from sources with zero Staff members in Facilities
     *
     * @return DTO without other Staff members
     */
    public StaffMemberDTO getDTOWithoutStaffMembers() {
        return new StaffMemberDTO(
                staffMemberId,
                person.getDTO(),
                StaffMember.mapDTOFromFacilityWithoutStaffMembers(worksAt),
                address.getDTO()
        );
    }

    /**
     * Maps Collection of Staff members to a Set of StaffMemberDTOs
     *
     * @param members Collection of Staff members
     * @return Set of StaffMemberDTOs
     */
    public static Collection<StaffMemberDTO> mapDTOFromStaffMembers(Collection<StaffMember> members) {
        return members.stream().map(StaffMember::getDTOWithoutStaffMembers).collect(Collectors.toSet());
    }

    /**
     * Creates Set of employees without Facilities to prevent recursion
     *
     * @return Set of DTO objects from employees
     */
    public static Set<StaffMemberDTO> mapDTOFromStaffMembersWithoutFacilities(Collection<StaffMember> employees) {
        return employees.stream().map(StaffMember::getDTOWithoutFacilities).collect(Collectors.toSet());
    }

    /**
     * Creates Set of Facilities without other Staff members to prevent recursion
     *
     * @return Set of DTO objects from Facilities
     */
    public static Set<FacilityDTO> mapDTOFromFacilityWithoutStaffMembers(Collection<Facility> facilities) {
        return facilities.stream().map(Facility::getDTOWithoutStaffMembers).collect(Collectors.toSet());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        StaffMember that = (StaffMember) o;
        return staffMemberId != null && Objects.equals(staffMemberId, that.staffMemberId);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
