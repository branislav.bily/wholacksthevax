package cz.cvut.fit.bilybran.wholacksthevax.controller;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityView;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto.VaccinationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.service.facility.FacilityService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@AllArgsConstructor(onConstructor_ = {@Autowired})
@RequestMapping("api/facility")
public class FacilityController {

    private FacilityService service;

    @PostMapping
    @JsonView(FacilityView.Recursive.class)
    public FacilityDTO postFacility(@Validated(FacilityView.Recursive.class) @RequestBody FacilityDTO facilityDTO) {
        return service.postFacility(facilityDTO);
    }

    @GetMapping
    @JsonView(FacilityView.Recursive.class)
    public Collection<FacilityDTO> listAll() {
        return service.listAll();
    }

    @GetMapping("{id}")
    @JsonView(FacilityView.Recursive.class)
    public FacilityDTO getFacility(@PathVariable Long id) {
        return service.getFacility(id);
    }

    @GetMapping("{id}/tests")
    @JsonView(FacilityView.Recursive.class)
    public Collection<TestDTO> getTestsPerformed(@PathVariable Long id) {
        return service.getFacilityTests(id);
    }

    @GetMapping("{id}/vaccinations")
    @JsonView(FacilityView.Recursive.class)
    public Collection<VaccinationDTO> getVaccinationsPerformed(@PathVariable Long id) {
        return service.getVaccinations(id);
    }

    @PostMapping("{facility_id}/{member_id}")
    @JsonView(FacilityView.Recursive.class)
    public FacilityDTO addStaffMember(@PathVariable(value = "facility_id") Long facilityId, @PathVariable(value = "member_id") Long memberId) {
        return service.addMember(facilityId, memberId);
    }

    @DeleteMapping("{facility_id}/{member_id}")
    @JsonView(FacilityView.Recursive.class)
    public FacilityDTO removeStaffMember(@PathVariable(value = "facility_id") Long facilityId, @PathVariable(value = "member_id") Long memberId) {
        return service.removeMember(facilityId, memberId);
    }

    @GetMapping("{id}/employees")
    @JsonView(FacilityView.Recursive.class)
    public Collection<StaffMemberDTO> getFacilityEmployees(@PathVariable Long id) {
        return service.getFacilityEmployees(id);
    }

    @PutMapping("{id}")
    @JsonView(FacilityView.Recursive.class)
    public FacilityDTO updateFacility(@PathVariable Long id, @Validated(FacilityView.Recursive.class) @RequestBody FacilityDTO facilityDTO) {
        return service.updateFacility(id, facilityDTO);
    }

    @DeleteMapping("{id}")
    @JsonView(FacilityView.Recursive.class)
    public void deleteFacility(@PathVariable Long id) {
        service.deleteFacility(id);
    }
}
