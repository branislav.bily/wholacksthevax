package cz.cvut.fit.bilybran.wholacksthevax.model.test;

import cz.cvut.fit.bilybran.wholacksthevax.model.customer.Customer;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.Facility;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.StaffMember;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "test")
public class Test {

    @Id
    @GeneratedValue
    private Long testId;

    private Boolean result;

    private Date time;

    @ManyToOne
    private StaffMember performedBy;

    @ManyToOne
    private Facility performedAt;

    private TestType testType;

    @ManyToOne
    private Customer customer;

    /**
     * Converts DTO from Model
     *
     * @return DTO
     */
    public TestDTO getDTO() {
        return new TestDTO(
                testId,
                performedBy.getStaffMemberId(),
                performedAt.getFacilityId(),
                performedBy.getDTOWithoutFacilities(),
                performedAt.getDTOWithoutTestsAndVaccinations(),
                result,
                null,
                time,
                testType
        );
    }

    /**
     * Maps Collection of Tests to a Set of TestDTOs
     *
     * @param tests Collection of Tests
     * @return Set of TestDTOs
     */
    public static Set<TestDTO> mapDTOFromTests(Set<Test> tests) {
        return tests.stream().map(Test::getDTO).collect(Collectors.toSet());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Test test = (Test) o;
        return time.equals(test.time) && testType == test.testType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(time, testType);
    }
}
