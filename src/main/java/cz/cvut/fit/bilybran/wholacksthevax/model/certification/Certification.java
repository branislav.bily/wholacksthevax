package cz.cvut.fit.bilybran.wholacksthevax.model.certification;

import cz.cvut.fit.bilybran.wholacksthevax.model.certification.dto.CertificationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.Customer;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@ToString
@Table(name = "certification")
public class Certification {

    @Id
    @GeneratedValue
    private Long id;

    private CertificationType certificationType;

    private Date validUntil;

    @ManyToOne
    private Customer customer;

    private Long certificationOriginID;

    /**
     * Converts DTO from Model
     *
     * @return DTO
     */
    public CertificationDTO getDTO() {
        return new CertificationDTO(
                id,
                certificationType,
                null,
                validUntil,
                certificationOriginID
        );
    }

    /**
     * Maps Collection of Certifications to a Set of CertificationDTOs
     *
     * @param certifications Collection of Certifications
     * @return Set of CertificationDTOs
     */
    public static Set<CertificationDTO> mapDTOFromCertification(Set<Certification> certifications) {
        return certifications.stream().map(Certification::getDTO).collect(Collectors.toSet());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Certification that = (Certification) o;
        return certificationType == that.certificationType && validUntil.equals(that.validUntil);
    }

    @Override
    public int hashCode() {
        return Objects.hash(certificationType, validUntil);
    }
}
