package cz.cvut.fit.bilybran.wholacksthevax.model.address;

import cz.cvut.fit.bilybran.wholacksthevax.model.address.dto.AddressDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Embeddable
public class Address {

    @Column
    private String street;

    @Column
    private Integer streetNumber;

    @Column
    private String postalCode;

    @Column
    private String city;

    @Column
    private String state;

    /**
     * Converts DTO from Model
     *
     * @return DTO
     */
    public AddressDTO getDTO() {
        return new AddressDTO(
                street,
                streetNumber,
                postalCode,
                city,
                state
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(street, address.street) && Objects.equals(streetNumber, address.streetNumber) && Objects.equals(postalCode, address.postalCode) && Objects.equals(city, address.city) && Objects.equals(state, address.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(street, streetNumber, postalCode, city, state);
    }
}
