package cz.cvut.fit.bilybran.wholacksthevax.service.customer;


import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.customer.*;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.Authentication;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.certification.Certification;
import cz.cvut.fit.bilybran.wholacksthevax.model.certification.CertificationType;
import cz.cvut.fit.bilybran.wholacksthevax.model.certification.dto.CertificationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.Customer;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.Test;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.TestType;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.Vaccination;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class CertificationService {

    CustomerService service;

    /**
     * Returns all Certifications of Customer with given ID
     *
     * @param id Customer's identifier
     * @return Collection of all Certificates
     */
    public Collection<CertificationDTO> getCustomerCertification(Long id, AuthenticationDTO authenticationDTO) {
        Long authorizationId = service.authenticateCustomer(authenticationDTO);
        if (!authorizationId.equals(id)) {
            throw new IncorrectLoginCredentialsException();
        }
        System.out.println(Timestamp.from(Instant.now()) + "  Getting customer certifications with id " + id + " .");
        Customer customer = service.getCustomerById(id);
        return customer.getDTO().getCertificationDTOS();
    }

    /**
     * Posts new certification to a Customer with a given ID
     *
     * @param id               Customer's identifier
     * @param certificationDTO data source
     */
    @Transactional
    public void postCertification(Long id, Long originId, CertificationDTO certificationDTO) {
        service.authenticateCustomerWithID(certificationDTO.getAuthenticationDTO(), id);
        Date validUntil;
        Calendar c = Calendar.getInstance();
        //Certification is of Vaccination type
        if (certificationDTO.getCertificationType() == CertificationType.VACCINATION) {
            Set<Vaccination> vaccinations = service.getCustomerById(id).getVaccinations();
            //Check if test with given ID exists
            Optional<Vaccination> vaccination = vaccinations.stream().filter(v -> v.getVaccination_id().equals(originId)).findFirst();
            if (vaccination.isEmpty()) {
                throw new IncorrectVaccinationIDException();
            }
            c.setTime(vaccination.get().getAdministered());
            c.add(Calendar.YEAR, 1);
            //Certification is of Test type
        } else {
            Set<Test> tests = service.getCustomerById(id).getTests();
            //Check if test with given ID exists
            Optional<Test> opTest = tests.stream().filter(t -> t.getTestId().equals(originId)).findFirst();
            if (opTest.isEmpty()) {
                throw new IncorrectTestIDException();
            }
            Test test = opTest.get();
            if (test.getResult()) {
                throw new CannotCreateCertificationFromPositiveTestException();
            }
            //Since IDs are unique there should be only one test
            c.setTime(test.getTime());
            if (test.getTestType() == TestType.PCR) {
                c.add(Calendar.DAY_OF_MONTH, 1);
            } else {
                c.add(Calendar.DAY_OF_MONTH, 3);
            }
        }
        validUntil = c.getTime();
        System.out.println(Timestamp.from(Instant.now()) + "  Saving certification.");
        Customer customer = service.getCustomerById(id);
        //Set certification parameters
        Certification certification = certificationDTO.getCertification();
        certification.setCertificationOriginID(originId);
        certification.setValidUntil(validUntil);
        if (!customer.addCertification(certification)) {
            throw new CertificationAlreadyAddedException();
        }
        System.out.println(Timestamp.from(Instant.now()) + "  Certification saved.");
    }

    /**
     * Deletes Certification with given ID
     *
     * @param userId            Customer's identifier
     * @param certificationId   Certification's identifier
     * @param authenticationDTO login credentials for authentication
     */
    @Transactional
    public void deleteCertification(Long userId, Long certificationId, AuthenticationDTO authenticationDTO) {
        System.out.println(Timestamp.from(Instant.now()) + "  Deleting certification.");
        service.authenticateCustomerWithID(authenticationDTO, userId);
        Customer customer = service.getCustomerById(userId);
        Set<Certification> certifications = customer.getCertifications();
        Optional<Certification> certification = certifications.stream().filter(c -> c.getId().equals(certificationId)).findFirst();
        if (certification.isEmpty()) {
            throw new CertificationWithGivenIDDoesNotExists();
        }
        certifications.remove(certification.get());
        System.out.println(Timestamp.from(Instant.now()) + "  Certification deleted.");
    }
}
