package cz.cvut.fit.bilybran.wholacksthevax.model.vaccination;

import cz.cvut.fit.bilybran.wholacksthevax.model.facility.Facility;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.StaffMember;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto.VaccinationDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "vaccination")
public class Vaccination {

    @Id
    @GeneratedValue
    private Long vaccination_id;

    @ManyToOne
    private StaffMember performedBy;

    private VaccinationManufacturer manufacturer;

    private Date administered;

    @ManyToOne
    private Facility performedAt;

    /**
     * Converts DTO from Model
     * @return DTO
     */
    public VaccinationDTO getDTO() {
        return new VaccinationDTO(
                vaccination_id,
                performedBy.getStaffMemberId(),
                performedAt.getFacilityId(),
                performedBy.getDTOWithoutFacilities(),
                performedAt.getDTOWithoutFacilities(),
                null,
                manufacturer,
                administered
        );
    }

    /**
     * Maps Collection of Vaccinations to a Set of VaccinationDTOs
     *
     * @param vaccinations Collection of Vaccinations
     * @return Set of VaccinationsDTOs
     */
    public static Set<VaccinationDTO> mapDTOFromVaccinations(Set<Vaccination> vaccinations) {
        return vaccinations.stream().map(Vaccination::getDTO).collect(Collectors.toSet());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vaccination that = (Vaccination) o;
        return manufacturer == that.manufacturer && administered.equals(that.administered);
    }

    @Override
    public int hashCode() {
        return Objects.hash(manufacturer, administered);
    }
}
