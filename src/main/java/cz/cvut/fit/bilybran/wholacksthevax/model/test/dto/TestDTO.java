package cz.cvut.fit.bilybran.wholacksthevax.model.test.dto;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerView;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityView;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberView;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.Test;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.TestType;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Value
@AllArgsConstructor
@EqualsAndHashCode
public class TestDTO {
    @JsonView(value = {CustomerView.Tests.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    Long id;

    @NotNull
    Long performedById;

    @NotNull
    Long performedAtId;

    @JsonView(value = {CustomerView.Tests.class, FacilityView.Recursive.class})
    StaffMemberDTO performedBy;

    @JsonView(value = {CustomerView.Tests.class, StaffMemberView.Recursive.class})
    FacilityDTO performedAt;

    @JsonView(value = {CustomerView.Tests.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    @NotNull
    Boolean result;

    @NotNull
    @Valid
    AuthenticationDTO authenticationDTO;

    @JsonView(value = {CustomerView.Tests.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    @NotNull
    Date time;

    @JsonView(value = {CustomerView.Tests.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    @NotNull
    TestType testType;

    /**
     * Converts DTO into Model
     *
     * @return Model
     */
    public Test getTest() {
        return new Test(
                null,
                result,
                time,
                null,
                null,
                testType,
                null
        );
    }

    /**
     * Maps Collection of TestDTOs to a Set of Tests
     *
     * @param tests Collection of TestDTOs
     * @return Set of Tests
     */
    public static Set<Test> mapTestsFromDTO(Set<TestDTO> tests) {
        return tests.stream().map(TestDTO::getTest).collect(Collectors.toSet());
    }
}
