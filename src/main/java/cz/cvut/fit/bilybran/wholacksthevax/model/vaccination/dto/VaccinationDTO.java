package cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto;


import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerView;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityView;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberView;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.Vaccination;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.VaccinationManufacturer;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Value
@AllArgsConstructor
@EqualsAndHashCode
public class VaccinationDTO {

    @JsonView(value = {CustomerView.Vaccinations.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    Long id;

    @NotNull
    Long performedById;

    @NotNull
    Long performedAtId;

    @JsonView(value = {CustomerView.Vaccinations.class, FacilityView.Recursive.class})
    StaffMemberDTO performedBy;

    @JsonView(value = {CustomerView.Vaccinations.class, StaffMemberView.Recursive.class})
    FacilityDTO performedAt;

    @NotNull
    @Valid
    AuthenticationDTO authenticationDTO;

    @JsonView(value = {CustomerView.Vaccinations.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    @NotNull
    VaccinationManufacturer manufacturer;

    @JsonView(value = {CustomerView.Vaccinations.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    @NotNull
    Date administered;

    /**
     * Converts DTO into Model
     *
     * @return Model
     */
    public Vaccination getVaccination() {
        return new Vaccination(
                null,
                null,
                manufacturer,
                administered,
                null
        );
    }

    /**
     * Maps Collection of VaccinationDTOs to a Set of Vaccinations
     *
     * @param vaccinations Collection of VaccinationDTOs
     * @return Set of Vaccinations
     */
    public static Set<Vaccination> mapVaccinationsFromDTO(Collection<VaccinationDTO> vaccinations) {
        return vaccinations.stream().map(VaccinationDTO::getVaccination).collect(Collectors.toSet());
    }
}
