package cz.cvut.fit.bilybran.wholacksthevax.repository;

import cz.cvut.fit.bilybran.wholacksthevax.model.facility.Facility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacilityRepository extends JpaRepository<Facility, Long> {

    boolean existsByName(String name);
}
