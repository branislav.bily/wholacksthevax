package cz.cvut.fit.bilybran.wholacksthevax.model.customer;

import cz.cvut.fit.bilybran.wholacksthevax.model.address.Address;
import cz.cvut.fit.bilybran.wholacksthevax.model.certification.Certification;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Person;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.Test;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.Vaccination;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@ToString
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue
    private Long customer_id;

    @Column
    private String email;

    @Column
    private String password;

    @Embedded
    private Person person;

    @Embedded
    private Address address;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Test> tests;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Vaccination> vaccinations;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Certification> certifications;

    /**
     * Adds Certification into registry of Certifications
     *
     * @param certification new Certification
     * @return true if Certification was not previously in registry
     */
    public boolean addCertification(Certification certification) {
        return certifications.add(certification);
    }

    /**
     * Adds Vaccination into registry of Vaccinations
     *
     * @param vaccination new Vaccination
     * @return true if Vaccination was previously in registry
     */
    public boolean addVaccination(Vaccination vaccination) {
        return vaccinations.add(vaccination);
    }

    /**
     * Adds Test into registry of Tests
     *
     * @param test new Test
     * @return true if Test was previously in registry
     */
    public boolean addTest(Test test) {
        return tests.add(test);
    }

    /**
     * Converts DTO from Model
     *
     * @return DTO
     */
    public CustomerDTO getDTO() {
        return new CustomerDTO(
                customer_id,
                email,
                password,
                person.getDTO(),
                address.getDTO(),
                Test.mapDTOFromTests(tests),
                Vaccination.mapDTOFromVaccinations(vaccinations),
                Certification.mapDTOFromCertification(certifications));
    }

    /**
     * Maps Collection of Customers to a List of CustomerDTOs
     *
     * @param customers Collection of Customers
     * @return List of CustomerDTOs
     */
    public static List<CustomerDTO> mapCustomerToDTO(Collection<Customer> customers) {
        return customers.stream().map(Customer::getDTO).collect(Collectors.toList());
    }
}
