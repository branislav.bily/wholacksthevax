package cz.cvut.fit.bilybran.wholacksthevax.model.test;

public enum TestType {
    PCR,
    ANTIGEN
}
