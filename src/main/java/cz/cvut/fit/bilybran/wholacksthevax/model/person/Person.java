package cz.cvut.fit.bilybran.wholacksthevax.model.person;

import cz.cvut.fit.bilybran.wholacksthevax.model.person.dto.PersonDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Embeddable
public class Person {

    @Column
    private String firstName;

    @Column
    private String secondName;

    @Column
    private String socialSecurityNumber;

    @Column
    private Date dateOfBirth;

    @Column
    private Gender gender;

    @Column
    private Insurance insurance;

    /**
     * Converts DTO from Model
     *
     * @return DTO
     */
    public PersonDTO getDTO() {
        return new PersonDTO(
                firstName,
                secondName,
                socialSecurityNumber,
                dateOfBirth,
                gender,
                insurance
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(firstName, person.firstName) && Objects.equals(secondName, person.secondName) && Objects.equals(socialSecurityNumber, person.socialSecurityNumber) && Objects.equals(dateOfBirth, person.dateOfBirth) && gender == person.gender && insurance == person.insurance;
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, secondName, socialSecurityNumber, dateOfBirth, gender, insurance);
    }
}