package cz.cvut.fit.bilybran.wholacksthevax.model.vaccination;

public enum VaccinationManufacturer {
    PHIZER,
    ASTRA_ZENECA,
    MODERNA,
    JOHNSON
}
