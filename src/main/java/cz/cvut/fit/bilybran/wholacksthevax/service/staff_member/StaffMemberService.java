package cz.cvut.fit.bilybran.wholacksthevax.service.staff_member;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.FacilityWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.StaffMemberAlreadyWorksForFacilityException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberAlreadyExistsException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberDoesNotWorkForThisFacilityException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.Facility;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.StaffMember;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.Test;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.Vaccination;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto.VaccinationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.repository.FacilityRepository;
import cz.cvut.fit.bilybran.wholacksthevax.repository.StaffMemberRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;
import java.util.Optional;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class StaffMemberService {

    StaffMemberRepository repository;
    FacilityRepository facilityRepository;

    /**
     * Creates new Staff member
     *
     * @param staffMemberDTO StaffMemberDTO data source
     * @return new StaffMember
     */
    public StaffMemberDTO postMember(StaffMemberDTO staffMemberDTO) {
        System.out.println(Timestamp.from(Instant.now()) + "  Posting new member.");
        StaffMember staffMember = staffMemberDTO.getStaffMember();
        if (repository.existsByPersonAndAddress(staffMember.getPerson(), staffMember.getAddress())) {
            throw new StaffMemberAlreadyExistsException();
        }
        repository.save(staffMember);
        System.out.println(Timestamp.from(Instant.now()) + "  New member posted.");
        return staffMember.getDTOWithoutStaffMembers();
    }

    /**
     * Gets Staff member by a given ID
     *
     * @param id Staff member's identifier
     * @return Staff member with given ID
     */
    public StaffMemberDTO getStaffMember(Long id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Getting member with id " + id + ".");
        StaffMember staffMember = getStaffMemberById(id);
        return staffMember.getDTOWithoutStaffMembers();
    }

    /**
     * Lists all Staff members
     *
     * @return Collection of Staff members
     */
    public Collection<StaffMemberDTO> listAll() {
        System.out.println(Timestamp.from(Instant.now()) + "  Listing all staff members.");
        return StaffMember.mapDTOFromStaffMembers(repository.findAll());
    }

    /**
     * Adds Facility to a given Staff member
     *
     * @param memberId   Identification of Staff member from which Facility will be added
     * @param facilityId Identification of Facility
     * @return Staff member with added Facility
     */
    @Transactional
    public StaffMemberDTO addFacility(Long memberId, Long facilityId) {
        System.out.println(Timestamp.from(Instant.now()) + "  Adding facility to member with id " + memberId + ".");
        StaffMember staffMember = getStaffMemberById(memberId);
        Optional<Facility> facilityOptional = facilityRepository.findById(facilityId);
        if (facilityOptional.isEmpty()) {
            throw new FacilityWithGivenIDDoesNotExists();
        }
        Facility facility = facilityOptional.get();
        if (!staffMember.addFacility(facility)) {
            throw new StaffMemberAlreadyWorksForFacilityException();
        }
        facility.addStaffMember(staffMember);
        System.out.println(Timestamp.from(Instant.now()) + "  Facility added.");
        return staffMember.getDTOWithoutStaffMembers();
    }

    /**
     * Removes Facility from a given Staff member
     *
     * @param memberId   Identification of Staff member from which Facility will be added
     * @param facilityId Identification of Facility
     * @return Staff member with removed Facility
     */
    @Transactional
    public StaffMemberDTO removeFacility(Long memberId, Long facilityId) {
        System.out.println(Timestamp.from(Instant.now()) + "  Removing Facility.");
        StaffMember staffMember = getStaffMemberById(memberId);
        Optional<Facility> facilityOptional = facilityRepository.findById(facilityId);
        if (facilityOptional.isEmpty()) {
            throw new FacilityWithGivenIDDoesNotExists();
        }
        Facility facility = facilityOptional.get();
        if (!staffMember.worksAt(facility)) {
            throw new StaffMemberDoesNotWorkForThisFacilityException();
        }
        staffMember.removeFacility(facility);
        facility.removeMember(staffMember);
        System.out.println(Timestamp.from(Instant.now()) + "  Facility removed.");
        return staffMember.getDTOWithoutStaffMembers();
    }

    /**
     * Lists all colleagues of a Staff member in a given Facility
     *
     * @param memberId   Identification of Staff member
     * @param facilityId Identification of Facility
     * @return Collection of all colleagues
     */
    public Collection<StaffMemberDTO> getColleagues(Long memberId, Long facilityId) {
        System.out.println(Timestamp.from(Instant.now()) + "  Getting colleagues from facility with id " + facilityId + ".");
        StaffMember staffMember = getStaffMemberById(memberId);
        Optional<Facility> facilityOptional = facilityRepository.findById(facilityId);
        if (facilityOptional.isEmpty()) {
            throw new FacilityWithGivenIDDoesNotExists();
        }
        Facility facility = facilityOptional.get();
        Collection<StaffMember> colleagues = facility.getEmployees();
        //Removed itself from the list of colleagues
        colleagues.remove(staffMember);
        return StaffMember.mapDTOFromStaffMembers(colleagues);
    }

    /**
     * Lists all tests performed by a given Staff member
     *
     * @param id Identification of Staff member
     * @return Collection of all tests
     */
    public Collection<TestDTO> getTests(Long id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Getting tests of staff member with id " + id + ".");
        StaffMember member = getStaffMemberById(id);
        return Test.mapDTOFromTests(member.getTests());
    }

    /**
     * Lists all vaccinations performed by a given Staff member
     *
     * @param id Identification of Staff member
     * @return Collection of all vaccinations
     */
    public Collection<VaccinationDTO> getVaccinations(Long id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Getting vaccines of staff member with id " + id + ".");
        StaffMember member = getStaffMemberById(id);
        return Vaccination.mapDTOFromVaccinations(member.getVaccinations());
    }

    /**
     * Updates Staff member with given ID with new data from parameters
     *
     * @param id             Staff member's identifier
     * @param staffMemberDTO new Staff member data
     * @return updated Staff member
     */
    @Transactional
    public StaffMemberDTO updateStaffMember(Long id, StaffMemberDTO staffMemberDTO) {
        System.out.println(Timestamp.from(Instant.now()) + "  Updating staff member with id " + id.toString() + ".");
        StaffMember staffMember = getStaffMemberById(id);
        StaffMember newStaffMember = staffMemberDTO.getStaffMember();
        staffMember.setPerson(newStaffMember.getPerson());
        staffMember.setAddress(newStaffMember.getAddress());
        return staffMember.getDTOWithoutStaffMembers();
    }

    /**
     * Deletes Staff member with given ID
     *
     * @param id Staff member's identifier
     */
    public void deleteStaffMember(Long id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Deleting staff member.");
        StaffMember staffMember = getStaffMemberById(id);
        //First delete staff member from facilities
        if (!staffMember.getWorksAt().isEmpty()) {
            for (Facility facility : staffMember.getWorksAt()) {
                facility.removeMember(staffMember);
            }
        }
        //So that the facilities do not get deleted
        repository.delete(staffMember);
        System.out.println(Timestamp.from(Instant.now()) + "  Staff member deleted.");
    }

    /**
     * Gets Facility by a given ID
     *
     * @param id Facility's identifier
     * @return Staff member with given ID
     */
    public StaffMember getStaffMemberById(Long id) {
        Optional<StaffMember> staffMember = repository.findById(id);
        if (staffMember.isEmpty()) {
            throw new StaffMemberWithGivenIDDoesNotExists();
        }
        return staffMember.get();
    }
}
