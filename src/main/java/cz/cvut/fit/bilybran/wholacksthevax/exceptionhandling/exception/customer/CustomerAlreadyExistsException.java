package cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.customer;

//Exception when same Person with same Address wants to sign up under different email
public class CustomerAlreadyExistsException extends RuntimeException {
}
