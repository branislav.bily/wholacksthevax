package cz.cvut.fit.bilybran.wholacksthevax.model.address.dto;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.Address;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerView;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityView;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberView;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.NotEmpty;

@Value
@AllArgsConstructor
@EqualsAndHashCode
public class AddressDTO {

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    @NotEmpty(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    String street;

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    Integer streetNumber;

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    @NotEmpty(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    String postalCode;

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    @NotEmpty(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    String city;

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    @NotEmpty(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class, FacilityView.Recursive.class, StaffMemberView.Recursive.class})
    String state;

    /**
     * Converts DTO into Model
     *
     * @return Model
     */
    public Address getAddress() {
        return new Address(
                street,
                streetNumber,
                postalCode,
                city,
                state
        );
    }
}
