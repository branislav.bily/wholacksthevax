package cz.cvut.fit.bilybran.wholacksthevax.repository;

import cz.cvut.fit.bilybran.wholacksthevax.model.address.Address;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.Customer;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    boolean existsByEmail(String email);

    boolean existsByPersonAndAddress(Person person, Address address);

    boolean existsByEmailAndPassword(String email, String password);

    Customer findByEmailAndPassword(String email, String password);
}
