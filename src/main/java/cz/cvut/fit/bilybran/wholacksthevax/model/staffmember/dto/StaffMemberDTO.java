package cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto;


import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.dto.AddressDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerView;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityView;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.dto.PersonDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.StaffMember;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Set;

@Value
@AllArgsConstructor
@EqualsAndHashCode
public class StaffMemberDTO {

    @JsonView(value = {StaffMemberView.Recursive.class, FacilityView.Recursive.class, CustomerView.Tests.class, CustomerView.Vaccinations.class})
    Long id;

    @JsonView(value = {StaffMemberView.Recursive.class, FacilityView.Recursive.class, CustomerView.Tests.class, CustomerView.Vaccinations.class})
    @NotNull
    @Valid
    PersonDTO personDTO;

    @JsonView(value = {StaffMemberView.Recursive.class, FacilityView.NonRecursive.class})
    Set<FacilityDTO> worksAt;

    @JsonView(value = {StaffMemberView.Recursive.class, FacilityView.Recursive.class})
    @NotNull
    @Valid
    AddressDTO addressDTO;

    /**
     * Converts DTO into Model
     *
     * @return Model
     */
    public StaffMember getStaffMember() {
        return new StaffMember(
                null,
                personDTO.getPerson(),
                Collections.EMPTY_SET,
                addressDTO.getAddress(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
    }
}
