package cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.bilybran.wholacksthevax.hasher.StringHasher;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.dto.AddressDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.certification.dto.CertificationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.Customer;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.dto.PersonDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto.VaccinationDTO;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Value
@AllArgsConstructor
@EqualsAndHashCode
public class CustomerDTO {
    @JsonView(CustomerView.Info.class)
    Long id;

    @JsonView(value = {CustomerView.LogIn.class, CustomerView.Info.class})
    @Email(groups = {CustomerView.LogIn.class})
    @NotEmpty(groups = {CustomerView.LogIn.class})
    String email;

    @JsonView(value = {CustomerView.LogIn.class})
    @NotEmpty(groups = {CustomerView.LogIn.class})
    String password;

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, CustomerView.UpdatePersonAddress.class})
    @NotNull(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class})
    @Valid
    PersonDTO personDTO;

    @JsonView(value = {CustomerView.SignUp.class, CustomerView.Info.class, CustomerView.UpdatePersonAddress.class})
    @NotNull(groups = {CustomerView.SignUp.class, CustomerView.UpdatePersonAddress.class})
    @Valid
    AddressDTO addressDTO;

    @JsonView(CustomerView.Tests.class)
    @NotNull(groups = {CustomerView.Tests.class})
    @Valid
    Set<TestDTO> testDTOS;

    @JsonView(CustomerView.Vaccinations.class)
    @NotNull(groups = {CustomerView.Vaccinations.class})
    @Valid
    Set<VaccinationDTO> vaccinationDTOS;

    @JsonView(CustomerView.Certifications.class)
    @NotNull(groups = {CustomerView.Certifications.class})
    @Valid
    Set<CertificationDTO> certificationDTOS;


    /**
     * Converts DTO into Model
     *
     * @return Model
     */
    public Customer getCustomer() {
        return new Customer(
                null,
                email,
                password != null ? StringHasher.generateHash(password) : null,
                personDTO != null ? personDTO.getPerson() : null,
                addressDTO != null ? addressDTO.getAddress() : null,
                testDTOS != null ? TestDTO.mapTestsFromDTO(testDTOS) : null,
                vaccinationDTOS != null ? VaccinationDTO.mapVaccinationsFromDTO(vaccinationDTOS) : null,
                certificationDTOS != null ? CertificationDTO.mapCertificationFromDTO(certificationDTOS) : null
        );
    }
}
