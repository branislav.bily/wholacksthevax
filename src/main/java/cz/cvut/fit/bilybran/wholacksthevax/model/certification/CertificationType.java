package cz.cvut.fit.bilybran.wholacksthevax.model.certification;

public enum CertificationType {
    TEST,
    VACCINATION
}
