package cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto;

import cz.cvut.fit.bilybran.wholacksthevax.hasher.StringHasher;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.PasswordChange;
import lombok.AllArgsConstructor;
import lombok.Value;

import javax.validation.constraints.NotEmpty;

@Value
@AllArgsConstructor
public class PasswordChangeDTO {

    @NotEmpty
    String previousPassword;

    @NotEmpty
    String newPassword;

    /**
     * Converts DTO into Model
     *
     * @return Model
     */
    public PasswordChange getPasswordChange() {
        return new PasswordChange(
                StringHasher.generateHash(previousPassword),
                StringHasher.generateHash(newPassword)
        );
    }
}
