package cz.cvut.fit.bilybran.wholacksthevax.model.authentication;

import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class Authentication {

    String email;
    String password;

    /**
     * Creates DTO from Model
     *
     * @return DTO
     */
    public AuthenticationDTO getDTO() {
        return new AuthenticationDTO(
                email,
                password
        );
    }
}

