package cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.customer.*;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.FacilityWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.FacilityWithThisNameAlreadyExistsException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.StaffMemberAlreadyWorksForFacilityException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.hasher.IncorrectHashingAlgorithm;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberAlreadyExistsException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberDoesNotWorkForThisFacilityException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.StaffMember;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.stream.Collectors;

@ControllerAdvice
public class WhoLacksTheVaxExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = EmailAlreadyUsedException.class)
    public ResponseEntity<String> handleEmailAlreadyUsedException() {
        return new ResponseEntity<>("Email is already used", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = CustomerAlreadyExistsException.class)
    public ResponseEntity<String> handleCustomerAlreadyExistsException() {
        return new ResponseEntity<>("Customer already exists", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = IncorrectLoginCredentialsException.class)
    public ResponseEntity<String> handleIncorrectLoginCredentialsException() {
        return new ResponseEntity<>("Invalid login credentials", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UserWithGivenIDDoesNotExistsException.class)
    public ResponseEntity<String> handleUserWithGivenIDDoesNotExistsException() {
        return new ResponseEntity<>("User with given id does not exists", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = CertificationAlreadyAddedException.class)
    public ResponseEntity<String> handleCertificationAlreadyAddedException() {
        return new ResponseEntity<>("This exact certification was already added", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = CertificationWithGivenIDDoesNotExists.class)
    public ResponseEntity<String> handleCertificationWithGivenIDDoesNotExists() {
        return new ResponseEntity<>("Certification with given id does not exists", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = CannotCreateCertificationFromPositiveTestException.class)
    public ResponseEntity<String> handleCannotCreateCertificationFromPositiveTestException() {
        return new ResponseEntity<>("Can not create certification from a positive test", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = TestAlreadyAddedException.class)
    public ResponseEntity<String> handleTestAlreadyAddedException() {
        return new ResponseEntity<>("This exact test was already added", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = VaccinationAlreadyAddedException.class)
    public ResponseEntity<String> handleVaccinationAlreadyAddedException() {
        return new ResponseEntity<>("This exact vaccination was already added", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = IncorrectPasswordException.class)
    public ResponseEntity<String> handleIncorrectPasswordException() {
        return new ResponseEntity<>("Incorrect password", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = IncorrectVaccinationIDException.class)
    public ResponseEntity<String> handleIncorrectVaccinationIDException() {
        return new ResponseEntity<>("Customer does not have vaccination with given ID", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = IncorrectTestIDException.class)
    public ResponseEntity<String> handleIncorrectTestIDException() {
        return new ResponseEntity<>("Customer does not have test with given ID", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = IncorrectHashingAlgorithm.class)
    public ResponseEntity<String> handleIncorrectHashingAlgorithm() {
        return new ResponseEntity<>("An error occurred hashing your password", HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = FacilityWithThisNameAlreadyExistsException.class)
    public ResponseEntity<String> handleFacilityWithThisNameAlreadyExistsException() {
        return new ResponseEntity<>("Facility with given name already exists", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = FacilityWithGivenIDDoesNotExists.class)
    public ResponseEntity<String> handleFacilityWithGivenIDDoesNotExists() {
        return new ResponseEntity<>("Facility with given id does not exists", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = StaffMemberAlreadyWorksForFacilityException.class)
    public ResponseEntity<String> handleStaffMemberAlreadyWorksForFacilityException() {
        return new ResponseEntity<>("Staff member already works for this facility", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = StaffMemberAlreadyExistsException.class)
    public ResponseEntity<String> handleStaffMemberAlreadyExistsException() {
        return new ResponseEntity<>("Staff member already exists", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = StaffMemberWithGivenIDDoesNotExists.class)
    public ResponseEntity<String> handleStaffMemberWithGivenIDDoesNotExists() {
        return new ResponseEntity<>("Staff member with given id does not exists", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = StaffMemberDoesNotWorkForThisFacilityException.class)
    public ResponseEntity<String> handleStaffMemberDoesNotWorkForThisFacilityException() {
        return new ResponseEntity<>("Staff member with given id does not work for given facility", HttpStatus.NOT_FOUND);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String message = ex.getBindingResult().getFieldErrors().stream().map(er -> er.getField() + " : " + er.getDefaultMessage()).collect(Collectors.joining(", "));
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }
}
