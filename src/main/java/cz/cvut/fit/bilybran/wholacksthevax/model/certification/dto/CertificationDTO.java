package cz.cvut.fit.bilybran.wholacksthevax.model.certification.dto;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.certification.Certification;
import cz.cvut.fit.bilybran.wholacksthevax.model.certification.CertificationType;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerView;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Value
@AllArgsConstructor
@EqualsAndHashCode
public class CertificationDTO {

    @JsonView(CustomerView.Certifications.class)
    Long id;

    @JsonView(CustomerView.Certifications.class)
    @NotNull(groups = {CustomerView.Certifications.class})
    CertificationType certificationType;

    @NotNull(groups = {CustomerView.Certifications.class})
    @Valid
    AuthenticationDTO authenticationDTO;

    @JsonView(CustomerView.Certifications.class)
    Date validUntil;

    @JsonView(CustomerView.Certifications.class)
    Long certificationOriginID;

    /**
     * Converts DTO into Model
     *
     * @return Model
     */
    public Certification getCertification() {
        return new Certification(
                null,
                certificationType,
                validUntil,
                null,
                certificationOriginID
        );
    }

    /**
     * Maps Collection of CertificationDTOs to a Set of Certifications
     *
     * @param certifications Collection of CertificationDTOs
     * @return Set of Certifications
     */
    public static Set<Certification> mapCertificationFromDTO(Collection<CertificationDTO> certifications) {
        return certifications.stream().map(CertificationDTO::getCertification).collect(Collectors.toSet());
    }

}
