package cz.cvut.fit.bilybran.wholacksthevax.model.customer;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Getter
public class PasswordChange {

    String previousPassword;

    String newPassword;

}

