package cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto;

public interface FacilityView {

    interface NonRecursive extends Recursive {}

    interface Recursive {}
}
