package cz.cvut.fit.bilybran.wholacksthevax.service.facility;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.FacilityWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.FacilityWithThisNameAlreadyExistsException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.StaffMemberAlreadyWorksForFacilityException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberDoesNotWorkForThisFacilityException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.Facility;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.StaffMember;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.Test;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.Vaccination;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto.VaccinationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.repository.FacilityRepository;
import cz.cvut.fit.bilybran.wholacksthevax.repository.StaffMemberRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class FacilityService {

    FacilityRepository repository;
    StaffMemberRepository staffMemberRepository;

    /**
     * Creates new Facility
     *
     * @param facilityDTO FacilityDTO data source
     * @return new Facility
     */
    public FacilityDTO postFacility(FacilityDTO facilityDTO) {
        System.out.println(Timestamp.from(Instant.now()) + "  Post facility.");
        Facility facility = facilityDTO.getFacility();
        if (repository.existsByName(facility.getName())) {
            throw new FacilityWithThisNameAlreadyExistsException();
        }
        repository.save(facility);
        System.out.println(Timestamp.from(Instant.now()) + "  Facility saved.");
        return facility.getDTOWithoutFacilities();
    }

    /**
     * Gets Facility by a given ID
     *
     * @param id Facility's identifier
     * @return Facility with given ID
     */
    public FacilityDTO getFacility(Long id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Getting facility by id.");
        Facility facility = getFacilityById(id);
        return facility.getDTOWithoutFacilities();
    }

    /**
     * Lists all Facilities
     *
     * @return Collection of all Facilities
     */
    public Collection<FacilityDTO> listAll() {
        System.out.println(Timestamp.from(Instant.now()) + "  Listing all facilities.");
        return Facility.mapFacilitiesToDTO(repository.findAll());
    }

    /**
     * Adds Staff member to given Facility
     *
     * @param facilityId Identification of Facility from which staff member will be added
     * @param memberId   Identification of Staff member that will be added
     * @return Facility with added Staff member
     */
    @Transactional
    public FacilityDTO addMember(Long facilityId, Long memberId) {
        System.out.println(Timestamp.from(Instant.now()) + "  Adding member.");
        Facility facility = getFacilityById(facilityId);
        Optional<StaffMember> staffMember = staffMemberRepository.findById(memberId);
        if (staffMember.isEmpty()) {
            throw new StaffMemberWithGivenIDDoesNotExists();
        }
        if (!facility.addStaffMember(staffMember.get())) {
            throw new StaffMemberAlreadyWorksForFacilityException();
        }

        staffMember.get().addFacility(facility);
        System.out.println(Timestamp.from(Instant.now()) + "  Member added.");
        return facility.getDTOWithoutFacilities();
    }

    /**
     * Removes Staff member from a given Facility
     *
     * @param facility_id Identification of Facility from which staff member will be removed
     * @param member_id   Identification of Staff member that will be removed
     * @return Facility with removed Staff member
     */
    @Transactional
    public FacilityDTO removeMember(Long facility_id, Long member_id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Removing member.");
        Facility facility = getFacilityById(facility_id);
        Optional<StaffMember> staffMember = staffMemberRepository.findById(member_id);
        if (staffMember.isEmpty()) {
            throw new StaffMemberWithGivenIDDoesNotExists();
        }
        StaffMember member = staffMember.get();
        if (!facility.employs(member)) {
            throw new StaffMemberDoesNotWorkForThisFacilityException();
        }
        member.removeFacility(facility);
        facility.removeMember(member);
        System.out.println(Timestamp.from(Instant.now()) + "  Member removed.");
        return facility.getDTOWithoutFacilities();
    }

    /**
     * Lists all employees of a given Facility
     *
     * @param id Identification of Facility
     * @return List of all employees
     */
    public Collection<StaffMemberDTO> getFacilityEmployees(Long id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Getting employees of facility with id " + id + ".");
        Facility facility = getFacilityById(id);
        return StaffMember.mapDTOFromStaffMembers(facility.getEmployees());
    }

    /**
     * Lists all tests performed in given Facility
     *
     * @param id Identification of Facility
     * @return Collection of all tests
     */
    public Collection<TestDTO> getFacilityTests(Long id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Getting tests of facility with id " + id + ".");
        Facility facility = getFacilityById(id);
        return Test.mapDTOFromTests(facility.getTestsPerformed());
    }

    /**
     * Lists all vaccinations performed in given Facility
     *
     * @param id Identification of Facility
     * @return Collection of all vaccinations
     */
    public Collection<VaccinationDTO> getVaccinations(Long id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Getting vaccinations of facility with id " + id + ".");
        Facility facility = getFacilityById(id);
        return Vaccination.mapDTOFromVaccinations(facility.getVaccinationsPerformed());
    }

    /**
     * Updates Facility with given ID with new data from parameters
     *
     * @param id          Facility's identifier
     * @param facilityDTO new Facility data
     * @return updated Facility
     */
    @Transactional
    public FacilityDTO updateFacility(Long id, FacilityDTO facilityDTO) {
        System.out.println(Timestamp.from(Instant.now()) + "  Updating facility with id " + id.toString() + ".");
        Facility facility = getFacilityById(id);
        Facility newFacility = facilityDTO.getFacility();
        facility.setName(newFacility.getName());
        facility.setAddress(newFacility.getAddress());
        return facility.getDTOWithoutFacilities();
    }

    /**
     * Deletes Facility with given ID
     *
     * @param id Facility's identifier
     */
    @Transactional
    public void deleteFacility(Long id) {
        System.out.println(Timestamp.from(Instant.now()) + "  Deleting facility.");
        Facility facility = getFacilityById(id);
        //First delete facility from staff members
        if (!facility.getEmployees().isEmpty()) {
            for (StaffMember member : facility.getEmployees()) {
                member.removeFacility(facility);
            }
        }
        //So that the employees do not get deleted
        facility.setEmployees(Collections.EMPTY_SET);
        repository.delete(facility);
        System.out.println(Timestamp.from(Instant.now()) + "  Facility deleted.");
    }

    /**
     * Gets Facility by a given ID
     *
     * @param id Facility's identifier
     * @return Facility with given ID
     */
    public Facility getFacilityById(Long id) {
        Optional<Facility> facility = repository.findById(id);
        if (facility.isEmpty()) {
            throw new FacilityWithGivenIDDoesNotExists();
        }
        return facility.get();
    }
}
