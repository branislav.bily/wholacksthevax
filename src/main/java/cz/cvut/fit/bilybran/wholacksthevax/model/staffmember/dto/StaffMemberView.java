package cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto;

public interface StaffMemberView {

    interface NonRecursive extends Recursive {}

    interface Recursive {}
}
