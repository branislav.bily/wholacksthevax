package cz.cvut.fit.bilybran.wholacksthevax.hasher;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.hasher.IncorrectHashingAlgorithm;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class StringHasher {

    /**
     * Generates Hash using MessageDigest by implicitly using SHA-256 algorithm
     *
     * @param input that is going to be hashed
     * @return hashed String
     */
    public static String generateHash(String input) {
        //source: https://www.geeksforgeeks.org/sha-256-hash-in-java/
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            return generateHash(md, input);
        } catch (NoSuchAlgorithmException e) {
            throw new IncorrectHashingAlgorithm();
        }
    }

    /**
     * Generates Hash using MessageDigest and given algorithm from parameters
     *
     * @param input     that is going to be hashed
     * @param algorithm that will be used for hashing
     * @return hashed String
     */
    public static String generateHash(String input, String algorithm) {
        try {
            MessageDigest md = MessageDigest.getInstance(algorithm);
            return generateHash(md, input);
        } catch (NoSuchAlgorithmException e) {
            throw new IncorrectHashingAlgorithm();
        }
    }

    /**
     * Generates Hash using given MessageDigest
     *
     * @param md    used for generating hash
     * @param input that is going to be hashed
     * @return hashed String
     */
    private static String generateHash(MessageDigest md, String input) {
        BigInteger number = new BigInteger(1, md.digest(input.getBytes(StandardCharsets.UTF_8)));

        // Convert message digest into hex value
        StringBuilder hexString = new StringBuilder(number.toString(16));

        // Pad with leading zeros
        while (hexString.length() < 32) {
            hexString.insert(0, '0');
        }

        return hexString.toString();
    }
}
