package cz.cvut.fit.bilybran.wholacksthevax.model.person;

public enum Insurance {
    DOVERA,
    VSZP,
    UNION,
    EU_RESIDENCE,
    OTHER
}
