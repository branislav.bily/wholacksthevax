package cz.cvut.fit.bilybran.wholacksthevax.model.facility;

import cz.cvut.fit.bilybran.wholacksthevax.model.address.Address;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.StaffMember;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.Test;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.Vaccination;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Table(name = "facility")
public class Facility {

    @Id
    @GeneratedValue
    private Long facilityId;

    @Embedded
    private Address address;

    @Column
    private String name;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<StaffMember> employees;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Test> testsPerformed;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Vaccination> vaccinationsPerformed;

    /**
     * Adds Staff member into registry of employees
     *
     * @param member new employee
     * @return true if member was not previously employee
     */
    public boolean addStaffMember(StaffMember member) {
        return employees.add(member);
    }

    /**
     * Removes Staff member from registry of employees
     *
     * @param member employee that will be removed
     */
    public void removeMember(StaffMember member) {
        employees.remove(member);
    }

    /**
     * Adds Test into registry of Tests
     *
     * @param test new Test
     */
    public void addTest(Test test) {
        testsPerformed.add(test);
    }

    /**
     * Adds Vaccination into registry of Vaccinations
     *
     * @param vaccination new Vaccination
     */
    public void addVaccination(Vaccination vaccination) {
        vaccinationsPerformed.add(vaccination);
    }

    /**
     * Checks if Staff member is employee of Facility
     *
     * @param member Staff member that employment is in question
     * @return true if Staff member is an employee
     */
    public boolean employs(StaffMember member) {
        return employees.contains(member);
    }

    /**
     * Creates DTO from sources with zero Staff members
     *
     * @return DTO without employees
     */
    public FacilityDTO getDTOWithoutStaffMembers() {
        return new FacilityDTO(
                facilityId,
                name,
                address.getDTO(),
                Collections.EMPTY_SET,
                Test.mapDTOFromTests(testsPerformed)
        );
    }

    /**
     * Creates DTO from sources with empty Set as Facilities in employees
     *
     * @return DTO without other Facilities
     */
    public FacilityDTO getDTOWithoutFacilities() {
        return new FacilityDTO(
                facilityId,
                name,
                address.getDTO(),
                StaffMember.mapDTOFromStaffMembersWithoutFacilities(employees),
                Test.mapDTOFromTests(testsPerformed)
        );
    }

    /**
     * Creates DTO from sources with empty Set as Facilities in employees
     *
     * @return DTO without other Facilities
     */
    public FacilityDTO getDTOWithoutTestsAndVaccinations() {
        return new FacilityDTO(
                facilityId,
                name,
                address.getDTO(),
                StaffMember.mapDTOFromStaffMembersWithoutFacilities(employees),
                Collections.EMPTY_SET
        );
    }

    /**
     * Maps Collection of Facilities to a Set of FacilityDTOs
     *
     * @param facilities Collection of Facilities
     * @return Set of FacilityDTOs
     */
    public static Collection<FacilityDTO> mapFacilitiesToDTO(Collection<Facility> facilities) {
        return facilities.stream().map(Facility::getDTOWithoutFacilities).collect(Collectors.toSet());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Facility facility = (Facility) o;
        return facilityId != null && Objects.equals(facilityId, facility.facilityId);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
