package cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.dto.AddressDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerView;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.Facility;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberView;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import lombok.AllArgsConstructor;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.Set;

@Value
@AllArgsConstructor
public class FacilityDTO {

    @JsonView(value = {FacilityView.Recursive.class, StaffMemberView.Recursive.class, CustomerView.Tests.class, CustomerView.Vaccinations.class, StaffMemberView.Recursive.class, StaffMemberView.Recursive.class})
    Long id;

    @JsonView(value = {FacilityView.Recursive.class, StaffMemberView.Recursive.class, CustomerView.Tests.class, CustomerView.Vaccinations.class, StaffMemberView.Recursive.class, StaffMemberView.Recursive.class})
    @NotEmpty(groups = {FacilityView.Recursive.class})
    String name;

    @JsonView(value = {FacilityView.Recursive.class, StaffMemberView.Recursive.class, StaffMemberView.Recursive.class})
    @NotNull
    @Valid
    AddressDTO addressDTO;

    @JsonView(value = {FacilityView.Recursive.class})
    Set<StaffMemberDTO> employees;

    @JsonView(value = {FacilityView.Recursive.class})
    Set<TestDTO> tests;

    /**
     * Converts DTO into Model
     *
     * @return Model
     */
    public Facility getFacility() {
        return new Facility(
                null,
                addressDTO.getAddress(),
                name,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
    }

}
