package cz.cvut.fit.bilybran.wholacksthevax.service.customer;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.customer.TestAlreadyAddedException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberDoesNotWorkForThisFacilityException;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.Customer;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.Facility;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.StaffMember;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.Test;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import cz.cvut.fit.bilybran.wholacksthevax.service.facility.FacilityService;
import cz.cvut.fit.bilybran.wholacksthevax.service.staff_member.StaffMemberService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class TestService {

    CustomerService service;
    StaffMemberService staffMemberService;
    FacilityService facilityService;

    /**
     * Returns all Tests of Customer with given ID
     *
     * @param id Customer's identifier
     * @return Collection of all Tests
     */
    public Collection<TestDTO> getCustomerTests(Long id, AuthenticationDTO authenticationDTO) {
        service.authenticateCustomerWithID(authenticationDTO, id);
        System.out.println(Timestamp.from(Instant.now()) + "  Getting customer tests with id " + id.toString() + " .");
        Customer customer = service.getCustomerById(id);
        return customer.getDTO().getTestDTOS();
    }

    /**
     * Posts new test to a Customer with given ID
     *
     * @param id      Customer's identifier
     * @param testDTO data source
     */
    @Transactional
    public void postTest(Long id, TestDTO testDTO) {
        service.authenticateCustomerWithID(testDTO.getAuthenticationDTO(), id);
        System.out.println(Timestamp.from(Instant.now()) + "  Saving test.");
        Customer customer = service.getCustomerById(id);
        StaffMember staffMember = staffMemberService.getStaffMemberById(testDTO.getPerformedById());
        Facility facility = facilityService.getFacilityById(testDTO.getPerformedAtId());
        if (!facility.employs(staffMember)) {
            throw new StaffMemberDoesNotWorkForThisFacilityException();
        }
        Test test = testDTO.getTest();
        test.setPerformedBy(staffMember);
        test.setPerformedAt(facility);
        if (!customer.addTest(test)) {
            throw new TestAlreadyAddedException();
        }
        staffMember.addTest(test);
        facility.addTest(test);
        System.out.println(Timestamp.from(Instant.now()) + "  Test saved.");
    }
}
