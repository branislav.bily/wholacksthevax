package cz.cvut.fit.bilybran.wholacksthevax.model.person;

public enum Gender {
    MALE,
    FEMALE,
    RATHER_NOT_SAY
}
