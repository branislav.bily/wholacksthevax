package cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto;

import cz.cvut.fit.bilybran.wholacksthevax.hasher.StringHasher;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.Authentication;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerView;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Value
@AllArgsConstructor
@EqualsAndHashCode
public class AuthenticationDTO {

    @Email
    @NotEmpty
    String email;

    @NotEmpty
    String password;


    /**
     * Creates Model from DTO
     *
     * @return Model
     */
    public Authentication getModel() {
        return new Authentication(
                email,
                password != null ? StringHasher.generateHash(password) : null
        );
    }
}
