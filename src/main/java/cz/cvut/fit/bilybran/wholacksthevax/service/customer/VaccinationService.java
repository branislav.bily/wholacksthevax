package cz.cvut.fit.bilybran.wholacksthevax.service.customer;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.customer.VaccinationAlreadyAddedException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberDoesNotWorkForThisFacilityException;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.Customer;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.Facility;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.StaffMember;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.Vaccination;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto.VaccinationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.service.customer.CustomerService;
import cz.cvut.fit.bilybran.wholacksthevax.service.facility.FacilityService;
import cz.cvut.fit.bilybran.wholacksthevax.service.staff_member.StaffMemberService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@AllArgsConstructor(onConstructor_ = {@Autowired})
public class VaccinationService {

    CustomerService service;
    StaffMemberService staffMemberService;
    FacilityService facilityService;

    /**
     * Returns all Vaccinations of Customer with given ID
     *
     * @param id Customer's identifier
     * @return Collection of all Vaccinations
     */
    public Collection<VaccinationDTO> getCustomerVaccinations(Long id, AuthenticationDTO authenticationDTO) {
        service.authenticateCustomerWithID(authenticationDTO, id);
        System.out.println(Timestamp.from(Instant.now()) + "  Getting customer vaccinations with id " + id.toString() + " .");
        Customer customer = service.getCustomerById(id);
        return customer.getDTO().getVaccinationDTOS();
    }

    /**
     * Posts new vaccination to a Customer with a given ID
     *
     * @param id             Customer's identifier
     * @param vaccinationDTO data source
     */
    @Transactional
    public void postVaccination(Long id, VaccinationDTO vaccinationDTO) {
        service.authenticateCustomerWithID(vaccinationDTO.getAuthenticationDTO(), id);
        System.out.println(Timestamp.from(Instant.now()) + "  Saving vaccination.");
        Customer customer = service.getCustomerById(id);
        StaffMember staffMember = staffMemberService.getStaffMemberById(vaccinationDTO.getPerformedById());
        Facility facility = facilityService.getFacilityById(vaccinationDTO.getPerformedAtId());
        if (!facility.employs(staffMember)) {
            throw new StaffMemberDoesNotWorkForThisFacilityException();
        }
        Vaccination vaccination = vaccinationDTO.getVaccination();
        vaccination.setPerformedBy(staffMember);
        vaccination.setPerformedAt(facility);
        if (!customer.addVaccination(vaccination)) {
            throw new VaccinationAlreadyAddedException();
        }
        staffMember.addVaccination(vaccination);
        facility.addVaccination(vaccination);
        System.out.println(Timestamp.from(Instant.now()) + "  Vaccination saved.");
    }

}
