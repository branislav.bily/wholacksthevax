package cz.cvut.fit.bilybran.wholacksthevax.controller;

import com.fasterxml.jackson.annotation.JsonView;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.certification.dto.CertificationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerView;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.PasswordChangeDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto.VaccinationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.service.customer.CertificationService;
import cz.cvut.fit.bilybran.wholacksthevax.service.customer.CustomerService;
import cz.cvut.fit.bilybran.wholacksthevax.service.customer.TestService;
import cz.cvut.fit.bilybran.wholacksthevax.service.customer.VaccinationService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@AllArgsConstructor(onConstructor_ = {@Autowired})
@RequestMapping("api/customer")
public class CustomerController {

    private CustomerService service;
    private VaccinationService vaccinationService;
    private CertificationService certificationService;
    private TestService testService;

    @GetMapping
    @JsonView(CustomerView.Info.class)
    public Collection<CustomerDTO> listAll() {
        return service.listAll();
    }

    @GetMapping("/{id}")
    @JsonView(CustomerView.Info.class)
    public CustomerDTO getCustomer(@PathVariable Long id) {
        return service.getCustomer(id);
    }

    @PutMapping("/{id}")
    @JsonView(CustomerView.Info.class)
    public CustomerDTO updateCustomer(@PathVariable Long id, @RequestBody @Validated(CustomerView.UpdatePersonAddress.class) CustomerDTO customerDTO) {
        return service.updateCustomer(id, customerDTO);
    }

    @PutMapping("/{id}/password")
    public void updatePassword(@PathVariable Long id, @RequestBody @Validated PasswordChangeDTO passwordChangeDTO) {
        service.updatePassword(id, passwordChangeDTO);
    }

    @GetMapping("/{id}/certifications")
    @JsonView(CustomerView.Certifications.class)
    public Collection<CertificationDTO> getCustomerCertifications(@PathVariable Long id, @RequestBody @Valid AuthenticationDTO authenticationDTO) {
        return certificationService.getCustomerCertification(id, authenticationDTO);
    }

    @PostMapping("/{id}/certifications/{origin_id}")
    @JsonView(CustomerView.Certifications.class)
    public void postNewCertification(@PathVariable Long id, @PathVariable(value = "origin_id") Long originId, @RequestBody @Validated(CustomerView.Certifications.class) CertificationDTO certificationDTO) {
        certificationService.postCertification(id, originId, certificationDTO);
    }

    @DeleteMapping("/{id}/certifications/{certification_id}")
    public void deleteCertification(@PathVariable Long id, @PathVariable(value = "certification_id") Long certificationId, @RequestBody @Valid AuthenticationDTO authenticationDTO) {
        certificationService.deleteCertification(id, certificationId, authenticationDTO);
    }

    @GetMapping("/{id}/vaccinations")
    @JsonView(CustomerView.Vaccinations.class)
    public Collection<VaccinationDTO> getCustomerVaccinations(@PathVariable Long id, @RequestBody @Valid AuthenticationDTO authenticationDTO) {
        return vaccinationService.getCustomerVaccinations(id, authenticationDTO);
    }

    @PostMapping("/{id}/vaccinations")
    public void postNewVaccination(@PathVariable Long id, @RequestBody @Valid VaccinationDTO vaccinationDTO) {
        vaccinationService.postVaccination(id, vaccinationDTO);
    }

    @GetMapping("/{id}/tests")
    @JsonView(CustomerView.Tests.class)
    public Collection<TestDTO> getCustomerTests(@PathVariable Long id, @RequestBody @Valid AuthenticationDTO authenticationDTO) {
        return testService.getCustomerTests(id, authenticationDTO);
    }

    @PostMapping("/{id}/tests")
    @JsonView(CustomerView.Tests.class)
    public void postNewTest(@PathVariable Long id, @RequestBody @Valid TestDTO testDTO) {
        testService.postTest(id, testDTO);
    }

    @PostMapping("/signup")
    public void signUpCustomer(@RequestBody @Validated(CustomerView.SignUp.class) CustomerDTO customer) {
        service.signUpCustomer(customer);
    }

    @PostMapping("/login")
    public long loginCustomer(@RequestBody @Valid AuthenticationDTO authenticationDTO) {
        return service.authenticateCustomer(authenticationDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomer(@PathVariable Long id) {
        service.deleteCustomer(id);
    }
}
