package cz.cvut.fit.bilybran.wholacksthevax.service.staff_member;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.FacilityWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.FacilityWithThisNameAlreadyExistsException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberAlreadyExistsException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.Address;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.dto.AddressDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.Facility;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Gender;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Insurance;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Person;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.dto.PersonDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.StaffMember;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.TestType;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.Vaccination;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.VaccinationManufacturer;
import cz.cvut.fit.bilybran.wholacksthevax.repository.FacilityRepository;
import cz.cvut.fit.bilybran.wholacksthevax.repository.StaffMemberRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
public class StaffMemberServiceTest {

    @InjectMocks
    private StaffMemberService service;

    @Mock
    private StaffMemberRepository repository;

    @Mock
    private FacilityRepository facilityRepository;

    @Test
    void postMember() {
        StaffMemberDTO newStaff = new StaffMemberDTO(
                2L,
                new PersonDTO(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new AddressDTO(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                )
        );
        StaffMemberDTO oldStaff = new StaffMemberDTO(
                3L,
                new PersonDTO(
                        "Braniskko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new AddressDTO(
                        "Kupelnaa",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                )
        );

        Mockito.when(repository.existsByPersonAndAddress(
                oldStaff.getStaffMember().getPerson(),
                oldStaff.getStaffMember().getAddress()
        )).thenReturn(true);
        Mockito.when(repository.existsByPersonAndAddress(
                not(eq(oldStaff.getStaffMember().getPerson())),
                not(eq(oldStaff.getStaffMember().getAddress()))
        )).thenReturn(false);

        Throwable ex = assertThrows(StaffMemberAlreadyExistsException.class,
                () -> service.postMember(oldStaff));
        assertThat(ex).isInstanceOf(StaffMemberAlreadyExistsException.class);

        assertThat(service.postMember(newStaff))
                .extracting("personDTO")
                .extracting("firstName")
                .isEqualTo("Branko");
    }

    @Test
    void getStaffMemberById() {
        StaffMember member = new StaffMember(
                2L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(member));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(StaffMemberWithGivenIDDoesNotExists.class,
                () -> service.getStaffMemberById(2L));
        assertThat(ex).isInstanceOf(StaffMemberWithGivenIDDoesNotExists.class);

        assertThat(service.getStaffMemberById(1L)).isEqualTo(member);
    }

    @Test
    void getStaffMember() {
        StaffMember member = new StaffMember(
                2L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(member));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(StaffMemberWithGivenIDDoesNotExists.class,
                () -> service.getStaffMember(2L));
        assertThat(ex).isInstanceOf(StaffMemberWithGivenIDDoesNotExists.class);

        assertThat(service.getStaffMember(1L)).extracting("personDTO").extracting("firstName").isEqualTo("Branko");

    }

    @Test
    void listAll() {
        StaffMember member = new StaffMember(
                1L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        StaffMember member2 = new StaffMember(
                2L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        StaffMember member3 = new StaffMember(
                3L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.findAll()).thenReturn(List.of(member, member2, member3));

        assertThat(3).isEqualTo(service.listAll().size());
    }

    @Test
    void addFacility() {
        Facility facility = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                new HashSet<>(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        StaffMember member = new StaffMember(
                2L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                new HashSet<>(Set.of(facility)),
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Facility facility2 = new Facility(
                2L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                new HashSet<>(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(member));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());
        Mockito.when(facilityRepository.findById(3L)).thenReturn(Optional.of(facility2));

        Throwable ex = assertThrows(StaffMemberWithGivenIDDoesNotExists.class,
                () -> service.addFacility(2L, 3L));
        assertThat(ex).isInstanceOf(StaffMemberWithGivenIDDoesNotExists.class);

        assertThat(2).isEqualTo(service.addFacility(1L, 3L).getWorksAt().size());
    }

    @Test
    void removeFacility() {
        Facility facility = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                new HashSet<>(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        Facility facility2 = new Facility(
                2L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                new HashSet<>(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        StaffMember member = new StaffMember(
                2L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                new HashSet<>(Set.of(facility, facility2)),
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(member));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());
        Mockito.when(facilityRepository.findById(3L)).thenReturn(Optional.of(facility2));

        Throwable ex = assertThrows(StaffMemberWithGivenIDDoesNotExists.class,
                () -> service.removeFacility(2L, 3L));
        assertThat(ex).isInstanceOf(StaffMemberWithGivenIDDoesNotExists.class);

        assertThat(1).isEqualTo(service.removeFacility(1L, 3L).getWorksAt().size());
    }

    @Test
    void getColleagues() {
        Facility facility = new Facility(
                3L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                new HashSet<>(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        StaffMember member = new StaffMember(
                1L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                new HashSet<>(Set.of(facility)),
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        StaffMember member2 = new StaffMember(
                2L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                new HashSet<>(Set.of(facility)),
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        facility.addStaffMember(member);
        facility.addStaffMember(member2);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(member));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());
        Mockito.when(facilityRepository.findById(3L)).thenReturn(Optional.of(facility));

        Throwable ex = assertThrows(StaffMemberWithGivenIDDoesNotExists.class,
                () -> service.getColleagues(2L, 3L));
        assertThat(ex).isInstanceOf(StaffMemberWithGivenIDDoesNotExists.class);

        assertThat(1).isEqualTo(service.getColleagues(1L, 3L).size());
    }

    @Test
    void getTests() {
        StaffMember member = new StaffMember(
                1L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                new HashSet<>(Set.of()),
                Collections.EMPTY_SET
        );
        Facility facility = new Facility(
                3L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                new HashSet<>(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        cz.cvut.fit.bilybran.wholacksthevax.model.test.Test test = new cz.cvut.fit.bilybran.wholacksthevax.model.test.Test(
                1L,
                true,
                new Date(),
                member,
                facility,
                TestType.PCR,
                null
        );
        member.addTest(test);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(member));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(StaffMemberWithGivenIDDoesNotExists.class,
                () -> service.getTests(2L));
        assertThat(ex).isInstanceOf(StaffMemberWithGivenIDDoesNotExists.class);

        assertThat(1).isEqualTo(service.getTests(1L).size());
    }

    @Test
    void getVaccinations() {
        StaffMember member = new StaffMember(
                1L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                new HashSet<>()
        );
        Facility facility = new Facility(
                3L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Vaccination vaccine = new Vaccination(
                1L,
                member,
                VaccinationManufacturer.PHIZER,
                new Date(),
                facility
        );
        Vaccination vaccine2 = new Vaccination(
                2L,
                member,
                VaccinationManufacturer.ASTRA_ZENECA,
                new Date(),
                facility
        );
        member.addVaccination(vaccine);
        member.addVaccination(vaccine2);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(member));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(StaffMemberWithGivenIDDoesNotExists.class,
                () -> service.getVaccinations(2L));
        assertThat(ex).isInstanceOf(StaffMemberWithGivenIDDoesNotExists.class);

        assertThat(2).isEqualTo(service.getVaccinations(1L).size());
    }

    @Test
    void updateStaffMember() {
        StaffMember member = new StaffMember(
                1L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                new HashSet<>()
        );

        StaffMemberDTO newStaff = new StaffMemberDTO(
                1L,
                new PersonDTO(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new AddressDTO(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                )
        );

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(member));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(StaffMemberWithGivenIDDoesNotExists.class,
                () -> service.updateStaffMember(2L, newStaff));
        assertThat(ex).isInstanceOf(StaffMemberWithGivenIDDoesNotExists.class);

        assertThat(service.updateStaffMember(1L, newStaff))
                .usingRecursiveComparison()
                .isEqualTo(newStaff);
    }

    @Test
    void deleteStaffMember() {
        StaffMember member = new StaffMember(
                1L,
                new Person(
                        "Branko",
                        "Bily",
                        "kTomutoSaNebudemVyjadrovat",
                        new Date(),
                        Gender.RATHER_NOT_SAY,
                        Insurance.DOVERA
                ),
                Collections.EMPTY_SET,
                new Address(
                        "Kupelna",
                        1,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                new HashSet<>()
        );
        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(member));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(StaffMemberWithGivenIDDoesNotExists.class,
                () -> service.deleteStaffMember(2L));
        assertThat(ex).isInstanceOf(StaffMemberWithGivenIDDoesNotExists.class);

        assertDoesNotThrow(() -> service.deleteStaffMember(1L));
    }
}
