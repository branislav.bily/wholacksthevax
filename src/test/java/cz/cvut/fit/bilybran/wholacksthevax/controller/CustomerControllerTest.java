package cz.cvut.fit.bilybran.wholacksthevax.controller;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.customer.UserWithGivenIDDoesNotExistsException;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.Address;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.dto.AddressDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.certification.CertificationType;
import cz.cvut.fit.bilybran.wholacksthevax.model.certification.dto.CertificationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.Customer;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.PasswordChangeDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Gender;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Insurance;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Person;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.dto.PersonDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.TestType;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.VaccinationManufacturer;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto.VaccinationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.service.customer.CertificationService;
import cz.cvut.fit.bilybran.wholacksthevax.service.customer.CustomerService;
import cz.cvut.fit.bilybran.wholacksthevax.service.customer.TestService;
import cz.cvut.fit.bilybran.wholacksthevax.service.customer.VaccinationService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {

    @MockBean
    private CustomerService customerService;

    @MockBean
    private VaccinationService vaccinationService;

    @MockBean
    private TestService testService;

    @MockBean
    private CertificationService certificationService;

    @Autowired
    private MockMvc mvc;

    @Test
    void listAll() throws Exception {
        CustomerDTO branislav = new CustomerDTO(
                1L,
                "branislav.bily@gmail.com",
                //mojeHeslo
                "F01FAEF67D819EC1A2ABE7249D04F19068F5B717902DC0CFF05AE6D9C4739D02",
                new PersonDTO(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        CustomerDTO andrej = new CustomerDTO(
                2L,
                "ondrej.babis@gmail.com",
                //staleSomPremier
                "E5459FD0DC8661300A219DEA9A7EB138192439F027BC9C848E7325B7631F370A",
                new PersonDTO(
                        "Andrej",
                        "Babis",
                        "ToBySiChcelVediet",
                        new Date(30000000000L),
                        Gender.MALE,
                        Insurance.EU_RESIDENCE
                ),
                new AddressDTO(
                        "Prezidentsky palac",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        List<CustomerDTO> customers = List.of(branislav, andrej);

        Mockito.when(customerService.listAll()).thenReturn(customers);

        mvc.perform(get("/api/customer"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[0].email", Matchers.is("branislav.bily@gmail.com")))
                .andExpect(jsonPath("$[1].email", Matchers.is("ondrej.babis@gmail.com")));
    }

    @Test
    void getCustomer() throws Exception {
        CustomerDTO ja = new CustomerDTO(
                1L,
                "branislav.bily@gmail.com",
                //mojeHeslo
                "F01FAEF67D819EC1A2ABE7249D04F19068F5B717902DC0CFF05AE6D9C4739D02",
                new PersonDTO(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(customerService.getCustomer(1L)).thenReturn(ja);
        Mockito.when(customerService.getCustomer(not(eq(1L)))).thenThrow(UserWithGivenIDDoesNotExistsException.class);

        mvc.perform(get("/api/customer/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.email", Matchers.is("branislav.bily@gmail.com")));

        mvc.perform(get("/api/customer/2"))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteCustomer() throws Exception {
        Customer ja = new Customer(
                1L,
                "branislav.bily@gmail.com",
                //mojeHeslo
                "F01FAEF67D819EC1A2ABE7249D04F19068F5B717902DC0CFF05AE6D9C4739D02",
                new Person(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                new Address(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        Mockito.when(customerService.getCustomerById(1L)).thenReturn(ja);
        Mockito.when(customerService.getCustomerById(not(eq(1L)))).thenThrow(UserWithGivenIDDoesNotExistsException.class);

        mvc.perform(delete("/api/customer/1"))
                .andExpect(status().isOk());
        verify(customerService, times(1)).deleteCustomer(1L);
    }

    @Test
    void updateCustomer() throws Exception {
        mvc.perform(put("/api/customer/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"personDTO\": {\n" +
                                "\t\t\"firstName\": \"Branislav\",\n" +
                                "\t\t\"secondName\": \"Bily\",\n" +
                                "\t\t\"socialSecurityNumber\": \"totoIsMojeSecurityNumber\",\n" +
                                "\t\t\"dateOfBirth\": \"2000-01-30T00:00:00.000+00:00\",\n" +
                                "\t\t\"gender\": \"MALE\",\n" +
                                "\t\t\"insurance\": \"VSZP\"\n" +
                                "\t},\n" +
                                "\t\"addressDTO\": {\n" +
                                "\t\t\"street\": \"jou muy\",\n" +
                                "\t\t\"streetNumber\": 2,\n" +
                                "\t\t\"postalCode\": \"08001\",\n" +
                                "\t\t\"city\": \"England\",\n" +
                                "\t\t\"state\": \"London\"\n" +
                                "\t}\n" +
                                "}"))
                .andExpect(status().isOk());

        mvc.perform(put("/api/customer/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"personDTO\": {\n" +
                                "\t\t\"firstName\": \"Branislav\",\n" +
                                "\t\t\"secondName\": \"Bily\",\n" +
                                "\t\t\"socialSecurityNumber\": \"totoIsMojeSecurityNumber\",\n" +
                                "\t\t\"dateOfBirth\": \"2000-01-30T00:00:00.000+00:00\",\n" +
                                "\t\t\"gender\": \"MALE\",\n" +
                                "\t},\n" +
                                "\t\"addressDTO\": {\n" +
                                "\t\t\"street\": \"jou muy\",\n" +
                                "\t\t\"streetNumber\": 2,\n" +
                                "\t\t\"postalCode\": \"08001\",\n" +
                                "\t\t\"city\": \"England\",\n" +
                                "\t\t\"state\": \"London\"\n" +
                                "\t}\n" +
                                "}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void signUpCustomer() throws Exception {
        mvc.perform(post("/api/customer/signup")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON + "; charset=utf-8")
                        .content("{\n" +
                                "\t\"email\": \"branislav.bily@gmail.com\",\n" +
                                "\t\"password\": \"mojeHesloSifrovane\",\n" +
                                "\t\"personDTO\": {\n" +
                                "\t\t\"firstName\": \"Branislav\",\n" +
                                "\t\t\"secondName\": \"Bily\",\n" +
                                "\t\t\"socialSecurityNumber\": \"totoIsMojeSecurityNumber\",\n" +
                                "\t\t\"dateOfBirth\": \"2000-01-30\",\n" +
                                "\t\t\"gender\": \"MALE\",\n" +
                                "\t\t\"insurance\": \"DOVERA\"\n" +
                                "\t},\n" +
                                "\t\"addressDTO\": {\n" +
                                "\t\t\t\"street\": \"jou muy\",\n" +
                                "\t\t\t\"city\": \"England\",\n" +
                                "\t\t\t\"postalCode\": \"08001\",\n" +
                                "\t\t\t\"streetNumber\": 2,\n" +
                                "\t\t\t\"state\": \"London\"\n" +
                                "\t\t}\n" +
                                "}"))
                .andExpect(status().isOk());
    }

    @Test
    void logInCustomer() throws Exception {
        AuthenticationDTO auth = new AuthenticationDTO(
                "email@gmail.com",
                "whatever"
        );
        Mockito.when(customerService.authenticateCustomer(auth)).thenReturn(1L);

        mvc.perform(post("/api/customer/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"email\": \"email@gmail.com\",\n" +
                        "\t\"password\": \"whatever\"\n" +
                        "}"))
                .andExpect(jsonPath("$", Matchers.is(1)));
    }

    @Test
    void updatePassword() throws Exception {
        PasswordChangeDTO pass = new PasswordChangeDTO(
                "mojeStare",
                "mojeNove"
        );

        //No idea what to do here
        mvc.perform(put("/api/customer/1/password")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"previousPassword\": \"mojeStare\",\n" +
                        "\t\"newPassword\": \"mojeNove\"\n" +
                        "}"))
                .andExpect(status().isOk());
    }

    @Test
    void getCustomerCertifications() throws Exception {
        CertificationDTO cer = new CertificationDTO(
                1L,
                CertificationType.TEST,
                null,
                new Date(),
                10L
        );

        CertificationDTO cer1 = new CertificationDTO(
                2L,
                CertificationType.VACCINATION,
                null,
                new Date(),
                10L
        );

        Set<CertificationDTO> certificationDTOSet = Set.of(cer, cer1);
        AuthenticationDTO auth = new AuthenticationDTO(
                "branislav.bily@gmail.com",
                "noveHesloSifrovane"
        );

        Mockito.when(certificationService.getCustomerCertification(1L, auth)).thenReturn(certificationDTOSet);

        mvc.perform(get("/api/customer/1/certifications")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"email\": \"branislav.bily@gmail.com\",\n" +
                        "\t\"password\": \"noveHesloSifrovane\"\n" +
                        "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    void postCustomerCertification() throws Exception {
        mvc.perform(post("/api/customer/1/certifications/10").contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"authenticationDTO\": {\n" +
                        "\t\t\"email\":\"branislav.bily@gmail.com\",\n" +
                        "\t\t\"password\":\"noveHesloSifrovane\"\n" +
                        "\t},\n" +
                        "\t\"certificationType\": \"TEST\"\n" +
                        "}"))
                .andExpect(status().isOk());
    }

    @Test
    void deleteCustomerCertification() throws Exception {
        mvc.perform(delete("/api/customer/1"))
                .andExpect(status().isOk());

        verify(customerService, times(1)).deleteCustomer(1L);
    }

    @Test
    void getCustomerTests() throws Exception {
        TestDTO cer = new TestDTO(
                1L,
                12L,
                13L,
                null,
                null,
                false,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                new Date(),
                TestType.PCR
        );

        TestDTO cer1 = new TestDTO(
                1L,
                12L,
                13L,
                null,
                null,
                false,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                new Date(),
                TestType.ANTIGEN
        );

        Set<TestDTO> testDTOS = Set.of(cer, cer1);
        AuthenticationDTO auth = new AuthenticationDTO(
                "branislav.bily@gmail.com",
                "noveHesloSifrovane"
        );

        Mockito.when(testService.getCustomerTests(1L, auth)).thenReturn(testDTOS);

        mvc.perform(get("/api/customer/1/tests")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"email\": \"branislav.bily@gmail.com\",\n" +
                                "\t\"password\": \"noveHesloSifrovane\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    void postCustomerTest() throws Exception {
        mvc.perform(post("/api/customer/1/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"authenticationDTO\": {\n" +
                        "\t\t\"email\":\"branislav.bily@gmail.com\",\n" +
                        "\t\t\"password\":\"noveHesloSifrovane\"\n" +
                        "\t},\n" +
                        "\t\"performedById\": 2,\n" +
                        "\t\"performedAtId\": 1,\n" +
                        "\t\"result\": \"true\",\n" +
                        "\t\"time\": \"2021-12-31\",\n" +
                        "\t\"testType\": \"PCR\"\n" +
                        "}")
        ).andExpect(status().isOk());
    }

    @Test
    void getCustomerVaccinations() throws Exception {
        VaccinationDTO v = new VaccinationDTO(
                1L,
                10L,
                12L,
                null,
                null,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                VaccinationManufacturer.PHIZER,
                new Date()
        );
        VaccinationDTO v1 = new VaccinationDTO(2L,
                10L,
                12L,
                null,
                null,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                VaccinationManufacturer.PHIZER,
                new Date()
        );

        AuthenticationDTO au = new AuthenticationDTO(
                "branislav.bily@gmail.com",
                        "mojeHeslo"
        );

        Set<VaccinationDTO> vs = Set.of(v, v1);

        Mockito.when(vaccinationService.getCustomerVaccinations(1L, au)).thenReturn(vs);

        mvc.perform(get("/api/customer/1/vaccinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"email\": \"branislav.bily@gmail.com\",\n" +
                        "\t\"password\": \"mojeHeslo\"\n" +
                        "}")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    void postCustomerVaccination() throws Exception {
        mvc.perform(post("/api/customer/1/vaccinations")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"authenticationDTO\": {\n" +
                        "\t\t\"email\": \"branislav.bily@gmail.com\",\n" +
                        "\t\t\"password\": \"noveHesloSifrovane\"\n" +
                        "\t},\n" +
                        "\t\"performedById\": 2,\n" +
                        "\t\"performedAtId\": 1,\n" +
                        "\t\"manufacturer\": \"PHIZER\",\n" +
                        "\t\"administered\": \"2021-01-20\"\n" +
                        "}"))
                .andExpect(status().isOk());

        mvc.perform(post("/api/customer/1/vaccinations")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"authenticationDTO\": {\n" +
                                "\t\t\"email\": \"branislav.bily@gmail.com\"\n" +
                                "\t},\n" +
                                "\t\"performedById\": 2,\n" +
                                "\t\"performedAtId\": 1,\n" +
                                "\t\"manufacturer\": \"PHIZER\",\n" +
                                "\t\"administered\": \"2021-01-20\"\n" +
                                "}"))
                .andExpect(status().isBadRequest());
    }
}