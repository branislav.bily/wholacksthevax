package cz.cvut.fit.bilybran.wholacksthevax.service.facility;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.FacilityWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.FacilityWithThisNameAlreadyExistsException;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.Address;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.dto.AddressDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.Facility;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Person;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.StaffMember;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.TestType;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.Vaccination;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.VaccinationManufacturer;
import cz.cvut.fit.bilybran.wholacksthevax.repository.FacilityRepository;
import cz.cvut.fit.bilybran.wholacksthevax.repository.StaffMemberRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
public class FacilityServiceTest {

    @InjectMocks
    private FacilityService service;

    @Mock
    private FacilityRepository repository;

    @Mock
    private StaffMemberRepository staffMemberRepository;

    @Test
    void postFacility() {

        FacilityDTO facilityDTO = new FacilityDTO(
                null,
                "Najlepsia",
                new AddressDTO(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.existsByName("Najlepsia")).thenReturn(true);

        Throwable ex = assertThrows(FacilityWithThisNameAlreadyExistsException.class,
                () -> service.postFacility(facilityDTO));
        assertThat(ex).isInstanceOf(FacilityWithThisNameAlreadyExistsException.class);

        FacilityDTO differentName = new FacilityDTO(
                null,
                "Najlepsiaa",
                new AddressDTO(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        assertThat(service.postFacility(differentName)).extracting("name").isEqualTo("Najlepsiaa");

    }

    @Test
    void getFacilityById() {
        Facility facility = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(facility));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(FacilityWithGivenIDDoesNotExists.class,
                () -> service.getFacilityById(2L));
        assertThat(ex).isInstanceOf(FacilityWithGivenIDDoesNotExists.class);

        assertThat(service.getFacilityById(1L)).isEqualTo(facility);
    }

    @Test
    void getFacility() {
        Facility facility = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(facility));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(FacilityWithGivenIDDoesNotExists.class,
                () -> service.getFacility(2L));
        assertThat(ex).isInstanceOf(FacilityWithGivenIDDoesNotExists.class);

        assertThat(service.getFacility(1L)).isEqualTo(facility.getDTOWithoutFacilities());
    }

    @Test
    void listAll() {
        Facility facility1 = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        Facility facility2 = new Facility(
                2L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Druha Najlepsia",
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        Facility facility3 = new Facility(
                3L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Tretia Najlepsia",
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        List<Facility> facilityList = List.of(facility1, facility2, facility3);

        Mockito.when(repository.findAll()).thenReturn(facilityList);

        assertThat(service.listAll()).containsExactlyInAnyOrder(
                facility1.getDTOWithoutFacilities(),
                facility2.getDTOWithoutFacilities(),
                facility3.getDTOWithoutFacilities()
        );


    }

    @Test
    void addMember() {

        StaffMember staffMember = new StaffMember(
                3L,
                new Person(),
                Collections.EMPTY_SET,
                new Address(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        StaffMember adding = new StaffMember(
                4L,
                new Person(),
                new HashSet<>(),
                new Address(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Facility facility = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                new HashSet<>(Set.of(staffMember)),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(facility));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());
        Mockito.when(staffMemberRepository.findById(3L)).thenReturn(Optional.of(adding));

        Throwable ex = assertThrows(FacilityWithGivenIDDoesNotExists.class,
                () -> service.addMember(2L, 3L));
        assertThat(ex).isInstanceOf(FacilityWithGivenIDDoesNotExists.class);

        assertThat(2).isEqualTo(service.addMember(1L, 3L).getEmployees().size());
    }

    @Test
    void removeMember() {
        StaffMember staffMember = new StaffMember(
                3L,
                new Person(),
                new HashSet<>(),
                new Address(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Facility facility = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                new HashSet<>(Set.of(staffMember)),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(facility));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());
        Mockito.when(staffMemberRepository.findById(2L)).thenReturn(Optional.of(staffMember));

        Throwable ex = assertThrows(FacilityWithGivenIDDoesNotExists.class,
                () -> service.removeMember(2L, 3L));
        assertThat(ex).isInstanceOf(FacilityWithGivenIDDoesNotExists.class);

        assertThat(0).isEqualTo(service.removeMember(1L, 2L).getEmployees().size());
    }

    @Test
    void getFacilityEmployees() {
        StaffMember staffMember = new StaffMember(
                3L,
                new Person(),
                Collections.EMPTY_SET,
                new Address(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        StaffMember staffMember2 = new StaffMember(
                4L,
                new Person(),
                Collections.EMPTY_SET,
                new Address(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        Facility facility = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                Set.of(staffMember, staffMember2),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(facility));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(FacilityWithGivenIDDoesNotExists.class,
                () -> service.getFacilityById(2L));
        assertThat(ex).isInstanceOf(FacilityWithGivenIDDoesNotExists.class);

        assertThat(2).isEqualTo(service.getFacilityEmployees(1L).size());
    }

    @Test
    void getFacilityTests() {
        StaffMember staffMember1 = new StaffMember(
                4L,
                new Person(),
                Collections.EMPTY_SET,
                new Address(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Facility facility = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                Collections.EMPTY_SET,
                new HashSet<>(),
                Collections.EMPTY_SET
        );
        cz.cvut.fit.bilybran.wholacksthevax.model.test.Test test = new cz.cvut.fit.bilybran.wholacksthevax.model.test.Test(
                1L,
                true,
                new Date(),
                staffMember1,
                facility,
                TestType.PCR,
                null
        );
        cz.cvut.fit.bilybran.wholacksthevax.model.test.Test test2 = new cz.cvut.fit.bilybran.wholacksthevax.model.test.Test(
                1L,
                true,
                new Date(),
                staffMember1,
                facility,
                TestType.ANTIGEN,
                null
        );

        facility.addTest(test);
        facility.addTest(test2);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(facility));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(FacilityWithGivenIDDoesNotExists.class,
                () -> service.getFacilityTests(2L));
        assertThat(ex).isInstanceOf(FacilityWithGivenIDDoesNotExists.class);

        assertThat(2).isEqualTo(service.getFacilityTests(1L).size());
    }

    @Test
    void getVaccinations() {
        StaffMember staffMember1 = new StaffMember(
                4L,
                new Person(),
                Collections.EMPTY_SET,
                new Address(),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Facility facility = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                new HashSet<>()
        );
        Vaccination vaccine = new Vaccination(
                1L,
                staffMember1,
                VaccinationManufacturer.PHIZER,
                new Date(),
                facility
        );
        Vaccination vaccine2 = new Vaccination(
                2L,
                staffMember1,
                VaccinationManufacturer.ASTRA_ZENECA,
                new Date(),
                facility
        );

        facility.addVaccination(vaccine);
        facility.addVaccination(vaccine2);

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(facility));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(FacilityWithGivenIDDoesNotExists.class,
                () -> service.getVaccinations(2L));
        assertThat(ex).isInstanceOf(FacilityWithGivenIDDoesNotExists.class);

        assertThat(2).isEqualTo(service.getVaccinations(1L).size());
    }

    @Test
    void updateFacility() {
        Facility facility = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        FacilityDTO facilityDTO = new FacilityDTO(
                1L,
                "Najlepsia vazne",
                new AddressDTO(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(facility));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(FacilityWithGivenIDDoesNotExists.class,
                () -> service.updateFacility(2L, facilityDTO));
        assertThat(ex).isInstanceOf(FacilityWithGivenIDDoesNotExists.class);

        assertThat(service.updateFacility(1L, facilityDTO))
                .usingRecursiveComparison()
                .isEqualTo(facilityDTO);
    }

    @Test
    void deleteFacility() {
        Facility facility = new Facility(
                1L,
                new Address(
                        "Kupelna",
                        2,
                        "08001",
                        "Presov",
                        "Slovakia"
                ),
                "Najlepsia",
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(repository.findById(1L)).thenReturn(Optional.of(facility));
        Mockito.when(repository.findById(not(eq(1L)))).thenReturn(Optional.empty());


        Throwable ex = assertThrows(FacilityWithGivenIDDoesNotExists.class,
                () -> service.deleteFacility(2L));
        assertThat(ex).isInstanceOf(FacilityWithGivenIDDoesNotExists.class);

         assertDoesNotThrow(() -> service.deleteFacility(1L));
    }
}
