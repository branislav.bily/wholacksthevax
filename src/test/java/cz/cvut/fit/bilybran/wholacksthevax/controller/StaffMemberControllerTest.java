package cz.cvut.fit.bilybran.wholacksthevax.controller;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.FacilityWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberDoesNotWorkForThisFacilityException;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.dto.AddressDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Gender;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Insurance;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.dto.PersonDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.TestType;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.VaccinationManufacturer;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto.VaccinationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.service.facility.FacilityService;
import cz.cvut.fit.bilybran.wholacksthevax.service.staff_member.StaffMemberService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;
import java.util.Set;

import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(StaffMemberController.class)
public class StaffMemberControllerTest {

    @MockBean
    private StaffMemberService service;

    @MockBean
    private FacilityService facilityService;

    @Autowired
    private MockMvc mvc;

    @Test
    void postStaffMember() throws Exception {
        mvc.perform(post("/api/staffMember")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"personDTO\": {\n" +
                        "\t\t\"firstName\": \"Ondrejiku\",\n" +
                        "\t\t\"secondName\": \"Osetrovatelny\",\n" +
                        "\t\t\"socialSecurityNumber\": \"totoIsMojeSecurityNumber\",\n" +
                        "\t\t\"dateOfBirth\": \"1990-03-30\",\n" +
                        "\t\t\"gender\": \"MALE\",\n" +
                        "\t\t\"insurance\": \"DOVERA\"\n" +
                        "\t},\n" +
                        "\t\"addressDTO\": {\n" +
                        "\t\t\t\"street\": \"Letna\",\n" +
                        "\t\t\t\"city\": \"Praha\",\n" +
                        "\t\t\t\"postalCode\": \"08001\",\n" +
                        "\t\t\t\"streetNumber\": 2,\n" +
                        "\t\t\t\"state\": \"Praha hl. mesto\"\n" +
                        "\t\t}\n" +
                        "}"))
                .andExpect(status().isOk());
    }

    @Test
    void listAll() throws Exception {
        StaffMemberDTO staffMemberDTO = new StaffMemberDTO(
                null,
                new PersonDTO(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                null,
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                )
        );
        StaffMemberDTO staffMemberDTO2 = new StaffMemberDTO(
                null,
                new PersonDTO(
                        "Branislavko",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                null,
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                )
        );

        Set<StaffMemberDTO> members = Set.of(staffMemberDTO, staffMemberDTO2);

        Mockito.when(service.listAll()).thenReturn(members);

        mvc.perform(get("/api/staffMember"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    void getStaffMember() throws Exception {
        StaffMemberDTO staffMemberDTO = new StaffMemberDTO(
                null,
                new PersonDTO(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                null,
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                )
        );

        Mockito.when(service.getStaffMember(1L)).thenReturn(staffMemberDTO);
        Mockito.when(service.getStaffMember(not(eq(1L)))).thenThrow(StaffMemberWithGivenIDDoesNotExists.class);

        mvc.perform(get("/api/staffMember/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.personDTO.firstName", Matchers.is("Branislav")));

        mvc.perform(get("/api/staffMember/2"))
                .andExpect(status().isNotFound());



    }

    @Test
    void getStaffMemberTests() throws Exception {
        TestDTO cer = new TestDTO(
                1L,
                12L,
                13L,
                null,
                null,
                false,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                new Date(),
                TestType.PCR
        );

        TestDTO cer1 = new TestDTO(
                1L,
                12L,
                13L,
                null,
                null,
                false,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                new Date(),
                TestType.ANTIGEN
        );

        Set<TestDTO> testDTOS = Set.of(cer, cer1);

        Mockito.when(service.getTests(1L)).thenReturn(testDTOS);
        Mockito.when(service.getTests(not(eq(1L)))).thenThrow(FacilityWithGivenIDDoesNotExists.class);

        mvc.perform(get("/api/staffMember/2/tests"))
                .andExpect(status().isNotFound());

        mvc.perform(get("/api/staffMember/1/tests"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    void getStaffMemberVaccinations() throws Exception {
        VaccinationDTO v = new VaccinationDTO(
                1L,
                10L,
                12L,
                null,
                null,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                VaccinationManufacturer.PHIZER,
                new Date()
        );
        VaccinationDTO v1 = new VaccinationDTO(2L,
                10L,
                12L,
                null,
                null,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                VaccinationManufacturer.PHIZER,
                new Date()
        );

        Set<VaccinationDTO> vs = Set.of(v, v1);

        Mockito.when(service.getVaccinations(1L)).thenReturn(vs);
        Mockito.when(service.getVaccinations(not(eq(1L)))).thenThrow(StaffMemberWithGivenIDDoesNotExists.class);

        mvc.perform(get("/api/staffMember/1/vaccinations")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)));

        mvc.perform(get("/api/staffMember/2/vaccinations")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isNotFound());
    }

    @Test
    void addFacility() throws Exception {

        FacilityDTO fac = new FacilityDTO(
                null,
                "Stvrta najsamlepsia facilitka",
                new AddressDTO(
                        "Dejvice",
                        2,
                        "08001",
                        "Praha",
                        "Praha hl. mesto"
                ),
                null,
                null
        );
        StaffMemberDTO staffMemberDTO = new StaffMemberDTO(
                null,
                new PersonDTO(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                Set.of(fac),
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                )
        );
        Mockito.when(service.getStaffMember(1L)).thenReturn(staffMemberDTO);
        Mockito.when(facilityService.getFacility(2L)).thenReturn(fac);

        Mockito.when(service.addFacility(1L,2L)).thenReturn(staffMemberDTO);

        mvc.perform(post("/api/staffMember/1/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.worksAt", Matchers.hasSize(1)));
    }

    @Test
    void removeFacility() throws Exception {
        mvc.perform(delete("/api/staffMember/1/2"))
                .andExpect(status().isOk());

        verify(service, times(1)).removeFacility(1L, 2L);
    }

    @Test
    void getColleagues() throws Exception {
        StaffMemberDTO staffMemberDTO1 = new StaffMemberDTO(
                null,
                new PersonDTO(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                null,
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                )
        );
        StaffMemberDTO staffMemberDTO2 = new StaffMemberDTO(
                null,
                new PersonDTO(
                        "Branislav",
                        "Bisadly",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                null,
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                )
        );
        StaffMemberDTO staffMemberDTO3 = new StaffMemberDTO(
                null,
                new PersonDTO(
                        "Branissadlav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                null,
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                )
        );
        Set<StaffMemberDTO> colleagues = Set.of(staffMemberDTO1, staffMemberDTO2, staffMemberDTO3);

        Mockito.when(service.getColleagues(1L, 2L)).thenReturn(colleagues);
        Mockito.when(service.getColleagues(not(eq(1L)), any())).thenThrow(StaffMemberDoesNotWorkForThisFacilityException.class);

        mvc.perform(get("/api/staffMember/1/2/colleagues"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(3)));

        mvc.perform(get("/api/staffMember/2/2/colleagues"))
                .andExpect(status().isNotFound());
    }

    @Test
    void updateStaffMember() throws Exception {
        mvc.perform(put("/api/staffMember/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t \"personDTO\": {\n" +
                        "      \"firstName\": \"Branislav\",\n" +
                        "      \"secondName\": \"Osetrovatelny\",\n" +
                        "      \"socialSecurityNumber\": \"totoIsMojeSecurityNumber\",\n" +
                        "      \"dateOfBirth\": \"1990-03-30T00:00:00.000+00:00\",\n" +
                        "      \"gender\": \"MALE\",\n" +
                        "      \"insurance\": \"VSZP\"\n" +
                        "    },\n" +
                        "    \"addressDTO\": {\n" +
                        "      \"street\": \"Letna\",\n" +
                        "      \"streetNumber\": 3,\n" +
                        "      \"postalCode\": \"08001\",\n" +
                        "      \"city\": \"Praha\",\n" +
                        "      \"state\": \"Praha hl. mesto\"\n" +
                        "    }\n" +
                        "}"))
                .andExpect(status().isOk());

        mvc.perform(put("/api/staffMember/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t \"personDTO\": {\n" +
                                "      \"firstName\": \"Branislav\",\n" +
                                "      \"secondName\": \"Osetrovatelny\",\n" +
                                "      \"socialSecurityNumber\": \"totoIsMojeSecurityNumber\",\n" +
                                "      \"dateOfBirth\": \"1990-03-30T00:00:00.000+00:00\",\n" +
                                "      \"insurance\": \"VSZP\"\n" +
                                "    },\n" +
                                "    \"addressDTO\": {\n" +
                                "      \"street\": \"Letna\",\n" +
                                "      \"streetNumber\": 3,\n" +
                                "      \"postalCode\": \"08001\",\n" +
                                "      \"city\": \"Praha\",\n" +
                                "      \"state\": \"Praha hl. mesto\"\n" +
                                "    }\n" +
                                "}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteStaffMember() throws Exception {
        mvc.perform(delete("/api/staffMember/1"))
                .andExpect(status().isOk());

        verify(service, times(1)).deleteStaffMember(1L);
    }
}
