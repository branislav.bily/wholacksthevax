package cz.cvut.fit.bilybran.wholacksthevax.service.customer;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.customer.*;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.Address;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.dto.AddressDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.Customer;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.CustomerDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.customer.dto.PasswordChangeDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Gender;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Insurance;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Person;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.dto.PersonDTO;
import cz.cvut.fit.bilybran.wholacksthevax.repository.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

    @InjectMocks
    private CustomerService customerService;

    @Mock
    private CustomerRepository customerRepository;

    @Test
    void signUpWithUsedEmail() {
        //Used email
        CustomerDTO branislav = new CustomerDTO(
                1L,
                "branislav.bily@gmail.com",
                //mojeHeslo
                "F01FAEF67D819EC1A2ABE7249D04F19068F5B717902DC0CFF05AE6D9C4739D02",
                new PersonDTO(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(customerRepository.existsByEmail(eq("branislav.bily@gmail.com"))).thenReturn(true);
        Throwable ex = assertThrows(EmailAlreadyUsedException.class, () -> customerService.signUpCustomer(branislav), "It should throw");
        assertThat(ex).isInstanceOf(EmailAlreadyUsedException.class);
    }

    @Test
    void signUpWithUsedPersonAndAddress() {
        //Used person and address
        CustomerDTO branislav = new CustomerDTO(
                1L,
                "branislav.bily@gmail.com",
                //mojeHeslo
                "F01FAEF67D819EC1A2ABE7249D04F19068F5B717902DC0CFF05AE6D9C4739D02",
                new PersonDTO(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(customerRepository.existsByEmail(any())).thenReturn(false);
        Mockito.when(customerRepository.existsByPersonAndAddress(any(), any())).thenReturn(true);
        Throwable ex = assertThrows(CustomerAlreadyExistsException.class, () -> customerService.signUpCustomer(branislav), "It should throw");
        assertThat(ex).isInstanceOf(CustomerAlreadyExistsException.class);
    }

    @Test
    void authenticateCustomer() {
        Customer customer = new Customer(
                1L,
                "branislav.bily@gmail.com",
                "mojeHesielko",
                null,
                null,
                null,
                null,
                null
        );
        AuthenticationDTO authenticationDTO = new AuthenticationDTO(
                "branislav.bily@gmail.com",
                "mojeHesielko"
        );

        Mockito.lenient().when(customerRepository.existsByEmailAndPassword("branislav.bily@gmail.com",
                                                                                    "493958a31196a66fd646645d4ea191d51204a0bae74ce198ee8f04ab77c04c90")).thenReturn(true);
        Mockito.when(customerRepository.findByEmailAndPassword(any(), any())).thenReturn(customer);
        assertThat(customerService.authenticateCustomer(authenticationDTO)).isEqualTo(1L);

        //Wrong password
        AuthenticationDTO joj = new AuthenticationDTO(
                "branislav.bily@gmail.com",
                "mojeHesiedlko"
        );

        Throwable ex = assertThrows(IncorrectLoginCredentialsException.class, () -> customerService.authenticateCustomer(joj));
        assertThat(ex).isInstanceOf(IncorrectLoginCredentialsException.class);

        assertThat(customerService.authenticateCustomer(authenticationDTO)).isEqualTo(1L);
    }

    @Test
    void authenticateCustomerWithId() {
        Customer customer = new Customer(
                1L,
                "branislav.bily@gmail.com",
                "mojeHesielko",
                null,
                null,
                null,
                null,
                null
        );
        AuthenticationDTO authenticationDTO = new AuthenticationDTO(
                "branislav.bily@gmail.com",
                "mojeHesielko"
        );

        Mockito.lenient().when(customerRepository.existsByEmailAndPassword(any(), any())).thenReturn(true);
        Mockito.when(customerRepository.findByEmailAndPassword(any(), any())).thenReturn(customer);
        Throwable ex = assertThrows(IncorrectLoginCredentialsException.class, () ->
                customerService.authenticateCustomerWithID(authenticationDTO, 2L));
        assertThat(ex).isInstanceOf(IncorrectLoginCredentialsException.class);
    }

    @Test
    void deleteCustomer() {
        Customer customer = new Customer(
                1L,
                "branislav.bily@gmail.com",
                "mojeHesielko",
                null,
                null,
                null,
                null,
                null
        );

        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));
        Mockito.when(customerRepository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(UserWithGivenIDDoesNotExistsException.class,
                () -> customerService.deleteCustomer(2L));
        assertThat(ex).isInstanceOf(UserWithGivenIDDoesNotExistsException.class);
        assertDoesNotThrow(() -> customerService.deleteCustomer(1L));
    }

    @Test
    void getCustomer() {
        Customer customer = new Customer(
                1L,
                "branislav.bily@gmail.com",
                "mojeHesielko",
                new Person(
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                new Address(
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));
        Mockito.when(customerRepository.findById(not(eq(1L)))).thenReturn(Optional.empty());


        Throwable ex = assertThrows(UserWithGivenIDDoesNotExistsException.class,
                () -> customerService.getCustomer(2L));
        assertThat(ex).isInstanceOf(UserWithGivenIDDoesNotExistsException.class);
        assertDoesNotThrow(() -> customerService.getCustomer(1L));
        assertThat(customerService.getCustomer(1L))
                .extracting("id").isEqualTo(1L);
    }

    @Test
    void listAll() {
        Customer customer = new Customer(
                1L,
                "branislavasd.bily@gmail.com",
                "mojeHesielko",
                new Person(
                        "Branci",
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                new Address(
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        Customer customer2 = new Customer(
                1L,
                "branislavasd.bily@gmail.com",
                "mojeHesielko",
                new Person(
                        "Branciko",
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                new Address(
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );
        List<Customer> customers = List.of(customer, customer2);
        Mockito.when(customerRepository.findAll()).thenReturn(customers);

        assertThat(customerService.listAll()).hasSize(2).containsExactly(
                customer.getDTO(),
                customer2.getDTO()
        );
    }

    @Test
    void updateCustomer() {
        Customer customer = new Customer(
                1L,
                "branislav.bily@gmail.com",
                "mojeHesielko",
                new Person(
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                new Address(
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));
        Mockito.when(customerRepository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        CustomerDTO customerDTO = new CustomerDTO(
                null,
                "branislav.bily@gmail.com",
                "mojeHesielko",
                new PersonDTO(
                        "Branci",
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                new AddressDTO(
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Throwable ex = assertThrows(UserWithGivenIDDoesNotExistsException.class,
                () -> customerService.updateCustomer(2L, customerDTO));
        assertThat(ex).isInstanceOf(UserWithGivenIDDoesNotExistsException.class);
        assertThat(customerService.updateCustomer(1L, customerDTO)).extracting("personDTO")
                .extracting("firstName").isEqualTo("Branci");
    }

    @Test
    void updatePassword() {
        Customer customer = new Customer(
                1L,
                "branislav.bily@gmail.com",
                "493958a31196a66fd646645d4ea191d51204a0bae74ce198ee8f04ab77c04c90",
                new Person(
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                new Address(
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        PasswordChangeDTO passwordChangeDTO = new PasswordChangeDTO(
                "mojeHesielko",
                "current"
        );

        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));
        Mockito.when(customerRepository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(UserWithGivenIDDoesNotExistsException.class,
                () -> customerService.updatePassword(2L, passwordChangeDTO));
        assertThat(ex).isInstanceOf(UserWithGivenIDDoesNotExistsException.class);

        ex = assertThrows(IncorrectPasswordException.class,
                () -> customerService.updatePassword(1L, new PasswordChangeDTO(
                        "zleHeslo",
                        "current"
                )));
        assertThat(ex).isInstanceOf(IncorrectPasswordException.class);

        assertDoesNotThrow(() -> customerService.updatePassword(1L, passwordChangeDTO));
    }

    @Test
    void getCustomerById() {
        Customer customer = new Customer(
                1L,
                "branislav.bily@gmail.com",
                "mojeHesielko",
                new Person(
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                new Address(
                        null,
                        null,
                        null,
                        null,
                        null
                ),
                Collections.EMPTY_SET,
                Collections.EMPTY_SET,
                Collections.EMPTY_SET
        );

        Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));
        Mockito.when(customerRepository.findById(not(eq(1L)))).thenReturn(Optional.empty());

        Throwable ex = assertThrows(UserWithGivenIDDoesNotExistsException.class,
                () -> customerService.getCustomerById(2L));
        assertThat(ex).isInstanceOf(UserWithGivenIDDoesNotExistsException.class);
        assertDoesNotThrow(() -> customerService.getCustomerById(1L));
        assertThat(customerService.getCustomerById(1L)).isEqualTo(customer);
    }
}
