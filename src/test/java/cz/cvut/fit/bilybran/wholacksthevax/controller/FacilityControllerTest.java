package cz.cvut.fit.bilybran.wholacksthevax.controller;

import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.facility.FacilityWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.exceptionhandling.exception.staff_member.StaffMemberWithGivenIDDoesNotExists;
import cz.cvut.fit.bilybran.wholacksthevax.model.address.dto.AddressDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.authentication.dto.AuthenticationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.facility.dto.FacilityDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Gender;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.Insurance;
import cz.cvut.fit.bilybran.wholacksthevax.model.person.dto.PersonDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.staffmember.dto.StaffMemberDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.TestType;
import cz.cvut.fit.bilybran.wholacksthevax.model.test.dto.TestDTO;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.VaccinationManufacturer;
import cz.cvut.fit.bilybran.wholacksthevax.model.vaccination.dto.VaccinationDTO;
import cz.cvut.fit.bilybran.wholacksthevax.service.facility.FacilityService;
import cz.cvut.fit.bilybran.wholacksthevax.service.staff_member.StaffMemberService;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Date;
import java.util.Set;

import static org.mockito.AdditionalMatchers.not;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(FacilityController.class)
public class FacilityControllerTest {

    @MockBean
    private FacilityService service;

    @MockBean
    private StaffMemberService staffMemberService;

    @Autowired
    private MockMvc mvc;

    @Test
    void postFacility() throws Exception {

        FacilityDTO fac = new FacilityDTO(
                null,
                "Stvrta najsamlepsia facilitka",
                new AddressDTO(
                        "Dejvice",
                        2,
                        "08001",
                        "Praha",
                        "Praha hl. mesto"
                ),
                null, null
        );
        Mockito.when(service.postFacility(fac)).thenReturn(fac);

        mvc.perform(post("/api/facility")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"name\": \"Stvrta najsamlepsia facilitka\",\n" +
                                "\t\"addressDTO\": {\n" +
                                "\t\t\t\"street\": \"Dejvice\",\n" +
                                "\t\t\t\"city\": \"Praha\",\n" +
                                "\t\t\t\"postalCode\": \"08001\",\n" +
                                "\t\t\t\"streetNumber\": 2,\n" +
                                "\t\t\t\"state\": \"Praha hl. mesto\"\n" +
                                "\t\t}\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.is("Stvrta najsamlepsia facilitka")));
    }

    @Test
    void listAll() throws Exception {
        FacilityDTO fac = new FacilityDTO(
                null,
                "Stvrta najsamlepsia facilitka",
                new AddressDTO(
                        "Dejvice",
                        2,
                        "08001",
                        "Praha",
                        "Praha hl. mesto"
                ),
                null, null
        );

        FacilityDTO fac2 = new FacilityDTO(
                null,
                "Prva najsamlepsia facilitka",
                new AddressDTO(
                        "Dejvice",
                        2,
                        "08001",
                        "Praha",
                        "Praha hl. mesto"
                ),
                null, null
        );

        Set<FacilityDTO> facilities = Set.of(fac, fac2);

        Mockito.when(service.listAll()).thenReturn(facilities);

        mvc.perform(get("/api/facility"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    void getFacility() throws Exception {
        FacilityDTO fac = new FacilityDTO(
                null,
                "Stvrta najsamlepsia facilitka",
                new AddressDTO(
                        "Dejvice",
                        2,
                        "08001",
                        "Praha",
                        "Praha hl. mesto"
                ),
                null, null
        );

        Mockito.when(service.getFacility(1L)).thenReturn(fac);
        Mockito.when(service.getFacility(not(eq(1L)))).thenThrow(FacilityWithGivenIDDoesNotExists.class);

        mvc.perform(get("/api/facility/2"))
                .andExpect(status().isNotFound());

        mvc.perform(get("/api/facility/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", Matchers.is("Stvrta najsamlepsia facilitka")));

    }

    @Test
    void getTestsPerformed() throws Exception {
        TestDTO cer = new TestDTO(
                1L,
                12L,
                13L,
                null,
                null,
                false,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                new Date(),
                TestType.PCR
        );

        TestDTO cer1 = new TestDTO(
                1L,
                12L,
                13L,
                null,
                null,
                false,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                new Date(),
                TestType.ANTIGEN
        );

        Set<TestDTO> testDTOS = Set.of(cer, cer1);

        Mockito.when(service.getFacilityTests(1L)).thenReturn(testDTOS);
        Mockito.when(service.getFacilityTests(not(eq(1L)))).thenThrow(FacilityWithGivenIDDoesNotExists.class);

        mvc.perform(get("/api/facility/2/tests"))
                .andExpect(status().isNotFound());

        mvc.perform(get("/api/facility/1/tests"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    void getVaccinationsPerformed() throws Exception {
        VaccinationDTO v = new VaccinationDTO(
                1L,
                10L,
                12L,
                null,
                null,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                VaccinationManufacturer.PHIZER,
                new Date()
        );
        VaccinationDTO v1 = new VaccinationDTO(2L,
                10L,
                12L,
                null,
                null,
                new AuthenticationDTO(
                        "branislav.bily@gmail.com",
                        "mojeHeslo"
                ),
                VaccinationManufacturer.PHIZER,
                new Date()
        );

        Set<VaccinationDTO> vs = Set.of(v, v1);

        Mockito.when(service.getVaccinations(1L)).thenReturn(vs);
        Mockito.when(service.getVaccinations(not(eq(1L)))).thenThrow(StaffMemberWithGivenIDDoesNotExists.class);

        mvc.perform(get("/api/facility/1/vaccinations")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)));

        mvc.perform(get("/api/facility/2/vaccinations")
                        .contentType(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isNotFound());
    }

    @Test
    void addStaffMember() throws Exception {

        FacilityDTO fac = new FacilityDTO(
                null,
                "Stvrta najsamlepsia facilitka",
                new AddressDTO(
                        "Dejvice",
                        2,
                        "08001",
                        "Praha",
                        "Praha hl. mesto"
                ),
                Collections.EMPTY_SET,
                null
        );

        StaffMemberDTO staffMemberDTO = new StaffMemberDTO(
                null,
                new PersonDTO(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                null,
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                )
        );

        FacilityDTO fac2 = new FacilityDTO(
                null,
                "Stvrta najsamlepsia facilitka",
                new AddressDTO(
                        "Dejvice",
                        2,
                        "08001",
                        "Praha",
                        "Praha hl. mesto"
                ),
                Set.of(staffMemberDTO),
                null
        );
        Mockito.when(service.getFacility(1L)).thenReturn(fac);
        Mockito.when(staffMemberService.getStaffMember(2L)).thenReturn(staffMemberDTO);

        Mockito.when(service.addMember(1L,2L)).thenReturn(fac2);

        mvc.perform(post("/api/facility/1/2"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.employees", Matchers.hasSize(1)));
    }

    @Test
    void removeStaffMember() throws Exception {
        StaffMemberDTO staffMemberDTO = new StaffMemberDTO(
                null,
                new PersonDTO(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                null,
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                )
        );
        FacilityDTO fac = new FacilityDTO(
                null,
                "Stvrta najsamlepsia facilitka",
                new AddressDTO(
                        "Dejvice",
                        2,
                        "08001",
                        "Praha",
                        "Praha hl. mesto"
                ),
                Set.of(staffMemberDTO),
                null
        );
        Mockito.when(service.getFacility(1L)).thenReturn(fac);
        Mockito.when(staffMemberService.getStaffMember(2L)).thenReturn(staffMemberDTO);

        mvc.perform(delete("/api/facility/1/2"))
                .andExpect(status().isOk());

        verify(service, times(1)).removeMember(1L, 2L);
    }

    @Test
    void getFacilityEmployees() throws Exception {
        StaffMemberDTO member1 = new StaffMemberDTO(
                null,
                new PersonDTO(
                        "Branislav",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                null,
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                )
        );
        StaffMemberDTO member2 = new StaffMemberDTO(
                null,
                new PersonDTO(
                        "Branislavko",
                        "Bily",
                        "ToBySiChcelVediet",
                        new Date(300000000L),
                        Gender.MALE,
                        Insurance.DOVERA
                ),
                null,
                new AddressDTO(
                        "Strahov",
                        2,
                        "BohZna",
                        "Prague",
                        "Praha"
                )
        );

        Set<StaffMemberDTO> members = Set.of(member1, member2);

        Mockito.when(service.getFacilityEmployees(1L)).thenReturn(members);

        mvc.perform(get("/api/facility/1/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    void updateFacility() throws Exception {
        mvc.perform(put("/api/facility/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"name\": \"Najsamlepsia facilitka v Praze\",\n" +
                        "\t\"addressDTO\": {\n" +
                        "\t\t\t\"street\": \"Dejvice\",\n" +
                        "\t\t\t\"city\": \"Praha\",\n" +
                        "\t\t\t\"postalCode\": \"08001\",\n" +
                        "\t\t\t\"streetNumber\": 2,\n" +
                        "\t\t\t\"state\": \"Praha hl. mesto\"\n" +
                        "\t\t}\n" +
                        "}"))
                .andExpect(status().isOk());

        mvc.perform(put("/api/facility/2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "\t\"name\": \"Najsamlepsia facilitka v Praze\",\n" +
                                "\t\"addressDTO\": {\n" +
                                "\t\t\t\"street\": \"Dejvice\",\n" +
                                "\t\t\t\"postalCode\": \"08001\",\n" +
                                "\t\t\t\"streetNumber\": 2,\n" +
                                "\t\t\t\"state\": \"Praha hl. mesto\"\n" +
                                "\t\t}\n" +
                                "}"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteFacility() throws Exception {
        mvc.perform(delete("/api/facility/1"))
                .andExpect(status().isOk());

        verify(service, times(1)).deleteFacility(1L);
    }
}
